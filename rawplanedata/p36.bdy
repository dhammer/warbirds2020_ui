CURTISS P-36C "HAWK"
-= Full weight =-
6237 lb (2829 kg)

-= Best speed and FTH =-
298 mph at sea level (480 kmh)
310 mph at 5700 ft (499 kmh at 1700 m)
313 mph at 10000 ft (504 kmh at 3050 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
6.6 min

-= Service Ceiling =-
30000 ft (9100 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
11.0 sec/lap, 372 ft radius

-= Power ON stall speed at 75% fuel =-
73 mph (117 kmh)

-= Max recommended dive speed (IAS) =-
445 mph (716 kmh)

-= Other notes =-
No pilot armor. No self sealing fuel tanks. Otherwise rugged structure.


The P-36C and Hawk 75 radial engine fighters were tooled for fighting below 10000 ft and could turn really well. Their performance quickly dropped off with altitude though and they struggled when up high. They had no pilot armor so the pilot was vulnerable to machine gun fire. With that said they were excellent low altitude fighters, able to turn with an A6M Zero when close to ground. 

The Curtiss P-36 Hawk, also known as the Curtiss Hawk Model 75, was an American-designed and built fighter aircraft of the 1930's and 40's. A contemporary of both the Hawker Hurricane and Messerschmitt Bf 109, it was one of the first of a new generation of combat aircraft—a sleek monoplane design making extensive use of metal in its construction and powered by a powerful radial engine. The P-36C was the American version with a Pratt and Whitney engine while the Hawk 75 was the export version with a very similar Wright engine.

Perhaps best known as the predecessor of the Curtiss P-40 Warhawk, the P-36 saw little combat with the United States Army Air Forces during World War II. The Hawk 75 version was nevertheless the fighter used most extensively and successfully by the French Armee de l'air during the Battle of France. It was also ordered by the governments of the Netherlands and Norway, but did not arrive in time to see action over either country before both were occupied by Nazi Germany. The type was also manufactured under license in China, for the Republic of China Air Force, as well as in British India, for the Royal Air Force (RAF) and Royal Indian Air Force (RIAF).