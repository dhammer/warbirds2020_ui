The M4A3 Sherman medium tank also had a five-man crew, a weight of 71,024 pounds, and a range of 100 miles. Its length with gun was 24 feet, eight inches, and the hull length was 20 feet, seven inches. Its width was eight feet, nine inches, and its height was 11 feet, 2.85 inches. 

Its armor plating was up to 3.94 inches, and a single 7.62mm coaxial machine gun complemented the 76mm main weapon. The powerplant consisted of a Ford GAA V8 gasoline engine developing 400-500 horsepower. Its maximum road speed was 30 miles per hour, and its fording ability was three feet. It could surmount a vertical obstacle two feet high and a trench 7 feet, five inches wide. 

Armament:                                           

Primary:  
1 x 76mm High-Velocity main gun.
1 x .50 caliber Browning M2 turret-mounted Anti-Aircraft (AA) machine gun.
1 x .30-06 caliber coaxial machine gun.
1 x .30-06 caliber bow-mounted machine gun.

Ammunition load:
95 x 75mm projectiles (estimated).
300 x .50 caliber ammunition.
4,750 x .30-06 caliber ammunition.
