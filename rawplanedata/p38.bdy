LOCKHEED P-38J "LIGHTNING"
-= Full weight =-
17513 lb (7944 kg)

-= Best speed and FTH =-
344 mph at sea level (554 kmh)
417 mph at 26000 ft (671 kmh at 7900 m)
412 mph at 29000 ft (663 kmh at 8800 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
5.5 min

-= Service Ceiling =-
39000 ft (11900 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
16.5 sec/lap, 739 ft radius

-= Power ON stall speed at 75% fuel =-
96 mph (154 kmh)

-= Max recommended dive speed (IAS) =-
445 mph (716 kmh)

-= Other notes =-
Excels at high altitudes, should avoid low alt fighting. Compresses above 400 mph. Slow roll rate. Counter rotating propellers.


The J-version had notably improved engines, the Allison V-1710-89/91, capable of producing 1570 hp. It has a slightly higher drag but is 10-13 mph faster than it's predecessors. Also the climb rate is improved due to it's better power/weight ratio. The P-38J can use rockets for attacking ground targets.

Developed by Kelly Johnson as a high altitude interceptor and escort fighter and built by Lockheed, the "Fork-Tailed Devil", or P-38, was an abrupt departure in design when its first prototype flew in 1939, and it continued to be an innovative aircraft through its illustrious career. With its twin engines, booms, and tails around a center fuselage, it was the first plane to enter squadron service with tricycle gear, all-metal flush riveted skin, turbo-supercharged engines, and power boosted controls. At the time it entered service it was the fastest and longest ranged fighter in the world.
 
Although it was hindered by technical and logistics problems and was gradually phased out in favor of the P-51 in Europe, the P-38 was an early mainstay in the Mediterranean and considered by many the premier fighter of the Pacific war.
 
America's two leading aces of WW2, Maj. Richard Bong (40 kills) and Maj. Tommy McGuire (38 kills) flew the P-38. Another P-38 claimed the life of Japan's Admiral Yamamoto when it shot down the transport carrying the Japanese commander. P-38's scored the final US air victories of the war when they downed six Ki-84 "Franks" on the day before the final ceasefire.
 
A total of 10,038 P-38's of all types were produced.