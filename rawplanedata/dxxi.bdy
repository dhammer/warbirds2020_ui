FOKKER D.XXI
-= Full weight =-
4519.5 lb (2050 kg)

-= Best speed and FTH =-
235 mph at sea level (378 kmh)
256 mph at 10000 ft (412 kmh at 3050 m)
270 mph at 17000 ft (435 kmh at 5200 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
5.6 min

-= Service Ceiling =-
32000 ft (9750 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
10.5 sec/lap, 326 ft radius

-= Power ON stall speed at 75% fuel =-
68 mph (109 kmh)

-= Max recommended dive speed (IAS) =-
435 mph (700 kmh)

-= Other notes =-
Fixed landing gear. Negative G's engine cutout.


The Fokker D.XXI was a Dutch plane that scored a fine tally against the Luftwaffe at the outbreak of the war. It did very well in several fights, but the Dutch pilots were simply to outnumbered to keep the Luftwaffe at bay. In Finland the D.XXI had a very impressive kill/loss ratio against the early Russian fighters. The Fokker D.XXI was lightweight but rugged and had thick wings. It was one of the very best turn-fighters of the second world war. The thick wings however, coupled with a non-retractable landing gear, took a heavy toll on it's top speed. The D.XXI was lightly armed, fielding 4x 7.7 mm machineguns.

It was a low-wing monoplane fighter aircraft. Following standard Fokker design practice of the period, it featured a welded steel tube fuselage that was largely covered by fabric, including the flight control surfaces; element forward of the trailing edges of the wings were covered by detachable aluminum panels instead. The wings were of a wooden construction, being composed of two box spars attached to ribs made of plywood. The aircraft was outfitted with a fixed spatted undercarriage with cantilever legs; braking was provided by independently-operated pedals using compress air.

The cockpit of the D.XXI was fully enclosed by a plexiglas hood featuring large sliding sections, and was entirely jettisonable in an emergency situation to enable pilots to bail out. Pilots were protected against turnover injuries by means of a pylon built into the structure of the aircraft set behind the seat.

Upon its entry to service in 1938, the D.XXI represented a significant leap forward for the Dutch Army Aviation Group, whose fighter force had until that time consisted of aging biplanes with open cockpits. The new Fokker quickly proved to be an extremely sturdy aircraft, being capable of attaining a speed of 700 km/h in a dive. 
