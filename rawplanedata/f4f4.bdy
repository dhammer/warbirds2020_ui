GRUMMAN F4F-4 "WILDCAT"
-= Full weight =-
7972 lb (3616 kg)

-= Best speed and FTH =-
296 mph at sea level (476 kmh)
294 mph at 4800 ft (473 kmh at 1500 m)
329 mph at 19000 ft (529 kmh at 5800 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
7.9 min

-= Service Ceiling =-
33000 ft (10100 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
14.9 sec/lap, 473 ft radius

-= Power ON stall speed at 75% fuel =-
78 mph (126 kmh)

-= Max recommended dive speed (IAS) =-
462 mph (744 kmh)

-= Other notes =-
Rugged fuselage.


The F4F-4 used the same engine as the F4F-3 and came with heavier armament but also added weight due to the extra guns and a new folding wing design, intended for saving space on the carrier deck. Many pilots prefered the lighter F4F-3 version which could climb and turn better against enemy fighters. For intercepting enemy bombers and strafing though the F4F-4 benefited from better firepower.

"A beer barrel on a roller skate, run through with an ironing board". - This is how one flier described the F4F/FM Wildcat. Outclassed in speed, maneuverability, and range by its primary adversary, the Japanese A6M2 Zero-Sen, the Wildcat nonetheless played an important role in the Pacific war. 
 
Built by Grumman, the F4F was an excellent example of the compromises required of carrier-borne fighters. Small and rugged, but underpowered and heavy for its size, this plane was originally beaten out by the Brewster F2A Buffalo in competition for a new monoplane fighter for the US Navy, but Grumman continued to improve the design and built 54 for the Navy in August 1939.
 
With its ability to withstand damage, Navy pilots were able to use tactics such as the "Thach Weave", developed by US Navy LCDR. Jimmy Thach, to keep this pugnacious warplane fighting. In the Thach Weave, a pair of Wildcats would fly abreast each other and weave back and forth. When an Zero saddled on one Wildcat's tail, his wingman would be in position to fire on the enemy as they crossed in front of him in the weave. When it needed to, the other advantage of the F4F over the Zero was its ability to dive faster, providing its pilots a ready escape should the situation turn bad.
 
Joseph Foss was the highest scoring Wildcat ace with 26 victories while flying from Guadalcanal in 1942. The top scoring FM-2 Ace was Lt. Ralph Elliot with 9 kills.
  
A total of 7316 F4F/FM Wildcats of all types were built for the US Navy and the British Royal Navy, entering service in 1940 and remaining the mainstay of the fleet fighter arm until the introduction of the F6F in 1943. Later, F4F/FM-2's were to serve on almost all of the US Navy's 114 escort carriers.
 
Wildcats had a kill to loss ratio of 6.9 to 1, though many of these kills were bombers and transports.