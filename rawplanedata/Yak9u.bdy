YAKOVLEV YAK-9U
-= Full weight =-
7040 lb (3193 kg)

-= Best speed and FTH =-
366 mph at sea level (589 kmh)
391 mph at 6000 ft (629 kmh at 1800 m)
415 mph at 15000 ft (668 kmh at 4600 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
4.5 min

-= Service Ceiling =-
35000 ft (10700 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
15.4 sec/lap, 719 ft radius

-= Power ON stall speed at 75% fuel =-
95 mph (153 kmh)

-= Max recommended dive speed (IAS) =-
447 mph (719 kmh)

-= Other notes =-
Good roll rate. High wingloading but fairly agile. Limited ammunition.


While the Yak-3 and Yak-9D might look similar, the Yak-3 was a significantly smaller aircraft both in shape, wing area and weight compared to the Yak-9D. The Yak-3 was more agile, faster and climbed better while the Yak-9D had more fuel for long range missions. They both used the Klimov VK-105PF engine but the Yak-3 used a version of it which allowed for a better max boost. The late 1944 Yakovlev version, the Yak-9U, was based on the heavier Yak-9D but used a much improved Klimov VK-107a engine, producing a maximum of 1650 hp compared to the 1300 hp of the Yak-3. The Yak-9U was considered the best in the series, and while being heavier and slightly less agile than the Yak-3, the stronger powerplant of the Yak-9U gave it a better top speed and power/weight ratio.

The Yak-9 was produced in larger numbers than any other Soviet fighter. Where the Yak-1 and Yak-3 were light point defense interceptors, the Yak-9, developed from the Yak-7, was a heavy multi-role aircraft.

With an increasing amount of materials being supplied by the United States, the Yak-7/Yak-9 line of fighters were able to make use of steel alloys and other materials unavailable to the designers of the Yak-1/Yak-3 line.

Deliveries of the Yak-9U to combat units started in Aug 1944, and at Dec 1944 the Klimov VK-107A engine was cleared for 3200 rpm (1650 hp). It mounted one 20mm cannon through the nose and two 12.7mm machine guns in the cowl.