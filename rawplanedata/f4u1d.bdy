VOUGHT F4U-1D "CORSAIR"
-= Full weight =-
12086 lb (5482 kg)

-= Best speed and FTH =-
354 mph at sea level (570 kmh)
400 mph at 14600 ft (644 kmh at 4450 m)
408 mph at 19500 ft (657 kmh at 5900 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
6.3 min

-= Service Ceiling =-
38000 ft (11600 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
16.7 sec/lap, 709 ft radius

-= Power ON stall speed at 75% fuel =-
96 mph (154 kmh)

-= Max recommended dive speed (IAS) =-
460 mph (740 kmh)

-= Other notes =-
Landing gear works as a speed break and can be deployed at high speeds. 9 minutes water injection capacity per sortie.


The F4U-1D also fields the R-2800-8W engine but carries notably less fuel and weighs in at 12086 lb, which is 726 lb lighter than the -1A. For long range operations the -1D can carry two droptanks and also has a good variety of ground attack ordnance with rockets and heavy bombs.

A marriage of the biggest, most powerful engine available with the smallest airframe that could hold it, the "Hawg" was a fearsome weapon. The Chance Vaught F4U-1D Corsair was one of the last variants of this capable fighter to see action in WW2.

A total of 9440 F4U-1's were produced. The first F4U-1A saw action in early 1943, and the F4U-1D entered service in mid 1944.
 
Designed as a carrier borne air-superiority fighter, the F4U-1 series early on was not an easy match for a carrier's deck. Nicknamed by some the "Ensign Eliminator", the torque effects of the powerful R-2800-8W engine, the difficult ground handling, and the limited visibility over the nose made carrier landings the province of experienced naval aviators only, and the F6F Hellcat was deemed a better choice for flight deck operations. 
 
However, when operating from land bases, the Corsair soon proved itself. Flown by land based US Marine and US Navy squadrons, the Corsair was a vast improvement over the earlier F4F and P-40, both slow, outdated fighters outclassed by the Japanese Zero. The Corsair was much faster, climbed far better, and was able to withstand tremendous damage. With its somewhat poor low speed handling this was not a plane for tight knife fights with Zeros. Its pilots flew it accordingly, attacking with speed, using its superior roll rate to evade attacks, and climbing out of reach while setting up the next pass.
 
The top scoring F4U ace was Marine Lt. Robert Hanson of VMF-215 with 25 victories in the plane, scoring 20 of these kills in a 13 day period. A Medal of Honor recipient, Lt. Hanson was killed by AAA during a strafing attack. The most famous Corsair pilot is probably Maj. Gregory "Pappy" Boyington, of the US Marine's VMF-214 Black Sheep squadron. He was credited with 22 of his 28 kills in the Corsair. The US Navy's VF-17 Jolly Rogers destroyed 154 Japanese aircraft and produced 12 aces in 79 days of combat in the "Hawg". Altogether, F4U's compiled an 11-1 kill to loss ratio.
 
Late in the war, Corsairs found their way onto carrier decks in numbers and proved one of the best defenses against Kamikaze attacks, using their great speed to chase down and destroy the suicidal Japanese planes before they could press home their attacks on US warships.