The Sd.Kfz. 10/4 (Sonderkraftfahrzeug - special motorized vehicle) was a German half-track that saw widespread use in World War II. Its main role was as a prime mover for small towed guns, such as the 2 cm FlaK 30, the 7.5 cm leIG, or the 3.7 cm PaK 36 anti-tank gun. It could carry eight troops in addition to towing a gun or trailer.

The basic engineering for all the German half-tracks was developed during the Weimar-era by the Reichswehr's Military Automotive Department, but final design and testing was farmed out to commercial firms with the understanding that production would be shared with multiple companies. Demag was chosen to develop the smallest of the German half-tracks and spent the years between 1934 and 1938 perfecting the design through a series of prototypes.

Armament:                                           
a prime mover for smaller towed guns, such as the 2 cm FlaK 30 or the 3.7 cm PaK 36 anti-tank gun, capable of carrying up to 8 soldiers in addition.

Ammunition load:
