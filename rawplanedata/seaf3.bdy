SUPERMARINE SEAFIRE III
-= Full weight =-
7300 lb (3311 kg)

-= Best speed and FTH =-
333 mph at sea level (536 kmh)
356 mph at 6000 ft (573 kmh at 1800 m)
322 mph at 25000 ft (518 kmh at 7600 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
6.4 min

-= Service Ceiling =-
34000 ft (10400 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
12.8 sec/lap, 505 ft radius

-= Power ON stall speed at 75% fuel =-
82 mph (132 kmh)

-= Max recommended dive speed (IAS) =-
450 mph (724 kmh)

-= Other notes =-
Agile with a gentle stall behaviour.


The Seafire III used the same Merlin 45M engine as the Seafire IIc, although a bit heavier than it's predecessor due to the new folding wings mechanism, which was introduced in order to save deck space aboard carriers. The Seafire III had an improved range of loadout options which made it better for strike missions.

In addition to being one of the most beautiful fighters ever built, the Supermarine Spitfire was one of the most robust designs. It had a great wing efficiency due to the eliptical shape of it's wings, resulting in less induced drag effects. With the same airframe that first flew as a prototype in 1936, the Spitfire remained in production throughout WW2, the only Allied fighter to do so. Improvements were mainly to the engine, with the basic shape remarkably able to accommodate the increased size and power of the ever larger Merlins. Combining speed, firepower, and agility into one potent package, the Spit with it's distinctive elliptical wings is always remembered as the plane that won the Battle of Britain... though the Hawker Hurricane carried most of the load.
 
The Spitfire Mk1a was in service at the outbreak of WW2. The Spit V became available in March 1941, and the Spit IX, the most produced variant at 5,665 planes, was introduced in mid 1942.
 
James "Johnny" Johnson was the highest scoring British Spitfire pilot with 38 victories.