2 x RR Merlin 72 Engines 

Full throttle height: 25,000ft  

Maximum take-off weight: 23,000 lbs 

Service Ceiling : Circa 37,000ft 

Recommended Climb Speed :  170mph IAS
 
Trims for take-off: 
  Rudder  -16   
  Elevator +8 

Flaps for take-off: 15-20 degrees  


The "Wooden Wonder" was rejected as a concept by Britain's Air Marshals, and had to be designed and built in an English manor house and test flown off an adjoining farm field. It became possibly the most successful and certainly most versatile British aircraft of WW2.

It served as Photo-recon, day fighter, night fighter, bomber, mine layer, torpedo bomber, tank buster, pathfinder, and submarine hunter. What's left?

Sir Geoffrey de Havilland proposed his wooden speed bomber in in 1938. When he was turned down, he said "We'll do it anyway". With the encouragement of Air Chief Marshal Sir Wilfred Freeman, de Havilland set up shop in Salisbury Hall and set to work. The prototype flew in November 1940 and went into production.

Wood was not a scarce commodity like the metals used in other planes, and much of the skilled workforce came from otherwise underemployed furniture craftsmen...making the Mosquito doubly valued, as much for its manufacturing efficiency as its performance.

7,781 Mosquitos of all types were eventually built. At near 400 mph level speed, it had the ability to penetrate deep into enemy territory while avoiding interception. In 1943, an 11am address in Berlin by Goering in which it was boasted that no enemy bomber would ever reach Berlin unscathed was interrupted by three Mosquitos from 105 squadron who dropped their bombs on the city and returned safely to English soil. At 4pm that same day, a similar address by Goebbels was interrupted in the same manner. It was an outstanding success, almost stage-managed by Goering himself, that gave the Mosquito a moral advantage it never relinquished.

In Warbirds, the Mosquito is a fast, powerful and annoying adversary. They are a devil to catch up with, and since they are most often used in "under the radar" low level raids, preparing for their arrival is next to impossible. They are great for deep raids into enemy territory and armed with 20mm cannon and four .303 machine guns all mounted in the nose, provide a fairly formidable opponent in a dogfight.