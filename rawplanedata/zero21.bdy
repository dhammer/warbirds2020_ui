MITSUBISHI A6M2 MODEL 21 "REISEN" (ZERO)
-= Full weight =-
5555 lb (2520 kg)

-= Best speed and FTH =-
283 mph at sea level (455 kmh)
305 mph at 6000 ft (491 kmh at 1800 m)
325 mph at 16000 ft (523 kmh at 4900 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
6.2 min

-= Service Ceiling =-
36000 ft (11000 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
11.0 sec/lap, 329 ft radius

-= Power ON stall speed at 75% fuel =-
69 mph (111 kmh)

-= Max recommended dive speed (IAS) =-
410 mph (660 kmh)

-= Other notes =-
Engine cut-out under to many negative G's. Loses aileron authority at high speeds. No pilot armor or self sealing fuel tanks.


With it's 990 hp Nakajima Sakae 12 engine the A6M2 held a nice top speed for it's era and could outturn nearly anything but a Ki-27b or a Fokker D.XXI. A disadvantage was the float carburetor of it's engine. While of a better design than the Merlin III carburetor, the engine of the A6M2 would still cut-out when under to many negative G's. American pilots also learned to exploit the Zero's tendency to lose aileron authority at high speeds in a dive. It was also susceptible to machine gun fire since it had no pilot armor or self sealing fuel tanks. That being said the A6M2 was one of the very best fighters of it's era, quite fast, agile, low stall speed and a great turnfighter that could duel with any opposition.

Designed in 1937, the Mitsubishi A6M "Zero" or "Zeke" fighter was revolutionary and unbeatable to its Chinese opponents when it was introduced in the summer of 1940. 15 pre-production test models all but ended all Chinese attempts to intercept Japanese bombers over mainland China. Designed as a carrier borne fighter from the start, the A6M was the best in the world during the first years of WW2. Had the Japanese won the early victory in the Pacific as they planned, the Zero may have been enough, but the war dragged on, and the Zero was soon outclassed by better and still better generations of enemy fighters whose numbers Japan could not match.
 
Production of the A6M series totalled approximately 10,449. The A6M2 was the model that first saw action in China, the A6M3 was introduced in July 1941, and the A6M5a saw service starting in Autumn, 1943.
 
Japanese pilots saw the plane as the modern successor to the Samurai's sword, and offensive capability was the main consideration. The Zero's turning ability was almost unmatched by any modern fighter, and it's range was no less than astounding. US planners frequently mistook far-ranging land based Zekes for signs of the presence of a Japanese carrier close by. Two 20mm cannon and two 7.7mm MG's gave it respectable firepower. But the Zero had it's shortcomings. The only front line naval fighter Japan used during most of the war, it's speed and climbing ability were no match for more modern fighters. It's light construction left it vulnerable to damage that would hardly affect the heavier opponents it faced, and even against the F4F, a plane it outperformed, this difference allowed superior team tactics by US fliers to defeat it. By the end of the war, Zero's were being used by Kamikaze pilots in the final defense of the home islands.
 
The top scoring Zero ace was Hiroyoshi Nishizawa. Nicknamed "The Devil" by his squadmates, he was credited with 113 victories although nobody knows an exact count for certain. Some figures place it lower, and some higher. His life came to an end when the transport plane he was flying in as a passenger was intercepted and shot down by American fighters.