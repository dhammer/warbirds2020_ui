SUPERMARINE SPITFIRE IXa
-= Full weight =-
7489.5 lb (3397 kg)

-= Best speed and FTH =-
314 mph at sea level (505 kmh)
378 mph at 15400 ft (608 kmh at 4700 m)
401 mph at 26500 ft (645 kmh at 8100 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
5.0 min

-= Service Ceiling =-
42000 ft (12800 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
13.7 sec/lap, 528 ft radius

-= Power ON stall speed at 75% fuel =-
83 mph (134 kmh)

-= Max recommended dive speed (IAS) =-
450 mph (724 kmh)

-= Other notes =-
Reduced negative G's engine cutout effect (Miss Shilling's orifice). Agile with a gentle stall behaviour.


Taking to the skies in mid 1942, the Spitfire IX series was Great Britains answer to the German FW 190's. The IXa has an improved powerplant, with it's Merlin 61 engine producing 1525 hp at 15400 ft. While being fairly equal to the Merlin 45 in max output at lower altitudes, it has a two stage supercharger with a much better high altitude performance.

In addition to being one of the most beautiful fighters ever built, the Supermarine Spitfire was one of the most robust designs. It had a great wing efficiency due to the eliptical shape of it's wings, resulting in less induced drag effects. With the same airframe that first flew as a prototype in 1936, the Spitfire remained in production throughout WW2, the only Allied fighter to do so. Improvements were mainly to the engine, with the basic shape remarkably able to accommodate the increased size and power of the ever larger Merlins. Combining speed, firepower, and agility into one potent package, the Spit with it's distinctive elliptical wings is always remembered as the plane that won the Battle of Britain... though the Hawker Hurricane carried most of the load.
 
The Spitfire Mk1a was in service at the outbreak of WW2. The Spit V became available in March 1941, and the Spit IX, the most produced variant at 5,665 planes, was introduced in mid 1942.
 
James "Johnny" Johnson was the highest scoring British Spitfire pilot with 38 victories.