NORTH AMERICAN MUSTANG MK1
-= Full weight =-
8625 lb (3912 kg)

-= Best speed and FTH =-
357 mph at sea level (575 kmh)
370 mph at 3500 ft (595 kmh at 1100 m)
367 mph at 12000 ft (591 kmh at 3700 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
8.0 min

-= Service Ceiling =-
29000 ft (8800 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
16.5 sec/lap, 647 ft radius

-= Power ON stall speed at 75% fuel =-
89 mph (143 kmh)

-= Max recommended dive speed (IAS) =-
480 mph (772 kmh)

-= Other notes =-
Agile at high speeds.


The prototype, designated NA-73X, first flew in October 1940 with the first production models flying in May 1941. This new aircraft was called the "Mustang" by the British, a name which would stick with the American variants as well. 

Although the early Allison engined models exhibited many fine characteristics and served as low altitude fighters, reconnaissance and dive bombers, the design came up short in its primary intended role as a high altitude interceptor. However the RAF soon found a useful role for this aircraft with the Army Cooperation Command. Flying low level tactical reconnaissance for the Army and long range "Rhubarb" missions these "first" Mustangs were in frontline use with the RAF all the way to the end of the war in May, 1945.