The first Merlin powered aircraft took to the sky in January 1945 and an initial production batch of 120 Tigers F Mk I was delivered from April 1945 onwards. Just like with the US Navy the type proved difficult to handle from aircraft carriers of the day and was mainly used from land based airfields.

The end of the war saw a large cutback in orders for new aircraft but in total 486 Tigers were produced in different versions. The last production version was the F Mk-VI of which 96 were delivered to the Royal Canadian Navy. 

Armament:                                           
primary armament was the deadly 88mm Kiwi 36 L/56 gun that was the most powerful antitank gun then in use by any army, capable of penetrating 112mm of armor plate from a range of 1400 meters.. 

Ammunition load:
104 x 75mm projectiles.
300 x 12.7mm ammunition.
4,750 x 7.62mm ammunition.
