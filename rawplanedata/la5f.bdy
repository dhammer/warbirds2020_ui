LAVOCHKIN LA-5F
-= Full weight =-
7114.3 lb (3227 kg)

-= Best speed and FTH =-
337 mph at sea level (542 kmh)
362 mph at 7000 ft (583 kmh at 2100 m)
372 mph at 21000 ft (599 kmh at 6400 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
5.1 min

-= Service Ceiling =-
32000 ft (9750 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
16.0 sec/lap, 691 ft radius

-= Power ON stall speed at 75% fuel =-
94 mph (151 kmh)

-= Max recommended dive speed (IAS) =-
389 mph (626 kmh)

-= Other notes =-
Good roll rate. High wingloading but quite agile.


Not the top, nor the worst fighter for it's era. The La-5F however finally gave the Russian pilots a fighting chance against the previously far superior German fighters in 1942-1943. It could almost match the speed of the Fw 190 at low altitude and was easily able to outturn it.

The Lavochkin La-5 was developed as a refinement of the LaGG-3 and became one of the most capable fighters of the war and perhaps the single best Russian fighter.

The La-5 and La-7 series aircraft essentially began with the LaGG-1, the underpowered aircraft went through many revisions and modifications to decrease weight, increase power and boost efficiency. That effort was what produced the Lavochkin line of aircraft. 

The La-5 series started when Semyon Lavochkin and Vladimir Gorbunov fitted a LaGG-3 with the new more powerful Shvetsov ASh-82 radial engine in the first half of 1942. Results from a March flight of the prototype proved interesting and worthy of further effort. Russian test pilots gave it rave reviews and work began in earnest in April of that year.

The new aircraft, designated La-5 would undergo more changes and upgrades eventually becoming the La-5F, La-5FN, La-5UTI (trainer version) and would lead to the birth of the La-7 series.

Nearly 10,000 La-5s would be produced before the end of the war, with some seeing service post war in many Eastern-Block nations.