The Boeing B-29 Superfortress is a four-engine propeller-driven heavy bomber designed by Boeing and was flown primarily by the United States during World War II and the Korean War. It was one of the largest aircraft operational during World War II and very advanced for its time. It featured a pressurized cabin, all dual wheeled, tricycle landing gears, and a remote, electronic fire-control system that controlled four machine gun turrets. 

A manned tail gun installation was semi-remote. The name "Superfortress" continued the pattern Boeing started with its well-known predecessor, the B-17 Flying Fortress. Designed for high-altitude strategic bomber role, the B-29 also excelled in low-altitude nighttime incendiary bombing missions. One of the B-29's final roles during World War II was carrying out the atomic bomb attacks on Hiroshima and Nagasaki.

Perhaps the most famous B-29s were the Silverplate series, which were modified to drop atomic bombs. They were also stripped of all guns except the tail gun to be lighter. The Silverplate aircraft were handpicked by Lieutenant Colonel Paul W. Tibbets for the mission, straight off the assembly line at the Omaha plant that was to become Offutt Air Force Base.

Enola Gay, flown by Tibbets, dropped the first bomb, called Little Boy, on Hiroshima on 6 August 1945. Enola Gay is fully restored and on display at the Smithsonian's Steven F. Udvar-Hazy Center, outside Dulles Airport in Washington, D.C. Bockscar dropped the second bomb, called Fat Man, on Nagasaki three days later. Bockscar is on display at the National Museum of the United States Air Force.

Following the surrender of Japan, called V-J Day, B-29s were used for other purposes. A number supplied POWs with food and other necessities by dropping barrels of rations on Japanese POW camps. In September 1945, a long-distance flight was undertaken for public relations purposes: Generals Barney M. Giles, Curtis LeMay, and Emmett O'Donnell, Jr. piloted three specially modified B-29s from Chitose Air Base in Hokkaido to Chicago Municipal Airport, continuing to Washington, D.C., the farthest nonstop distance to that date flown by U.S. Army Air Forces aircraft and the first-ever nonstop flight from Japan to the U.S. 

Two months later, Colonel Clarence S. Irvine commanded another modified B-29, Pacusan Dreamboat, in a world-record-breaking long-distance flight from Guam to Washington, D.C., traveling 7,916 miles (12,740 km) in 35 hours, with a gross takeoff weight of 155,000 pounds (70,000 kg). Almost a year later, in October 1946, the same B-29 flew 9,422 miles nonstop from Oahu, Hawaii to Cairo, Egypt in less than 40 hours, further proving the capability of routing airlines over the polar icecap. 


Armament:                     
Ammo Load           
Primary  : 4 x 50 Cal M2 Dual      4000 rpg            
Secondary: 1 x 50 Cal M2 Dual      2000 rpg   

Various bomb load
