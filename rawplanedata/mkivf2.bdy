The invasion of Russia in June 1941 revealed unequivocally the short-comings of the mark IV ausf D armed with the kwk/L24 75mm gun. Thus in Novemeber 1941 it was ordered to fit a longer high velocity antitank  gun in the Pzkpfw IV. First examples were ready by March,1942. 

Armed with the new 75mm KwK40/L43  the  Mark  IV ausf F2 could again compete on the battle field with the Soviet Unions T-34/76. Different stowage racks were designed to store the longer higher velocity ammunition designed  for this gun. All F2�s were built with the single baffle globular muzzle brake and given the designation F2. 

The Ausf F2  variant were based on the long version PzKpfw IV chassis  which was up armored over its predecessor, and when encountered in North Africa,the British nicknamed Ausf F2 . "Mark IV Special", since it was superior to any American or British tank at the time.
