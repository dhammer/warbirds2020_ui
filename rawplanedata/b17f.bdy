In July 1935, the prototype of the Boeing B-17 Bomber was rolled out and flown for the first time. When a newspaper writer at the event wrote of a "flying fortress", Boeing took note and the name stuck.
 
While it may not have been the best example of a heavy bomber in the war with its relatively small bombload, the B-17 certainly had the most impact. Sophisticated bombsights and daylight raids meant that the bombs they dropped hit target more often. The B-17 also had the ability to absorb tremendous amounts of enemy fire and return to base in England. Accounts of Forts returning minus half the tail or with 2 engines and a main fuel tank burned out or similarly horrific damage were common. Early unescorted B-17 raids into Germany were effective enough to cause the Luftwaffe to pull fighters from the Eastern front to put a stop to the daylight bombing. German pilots soon found the defensive fire put up by the close formations was brutal. But the B-17's lacked in frontal fire, and the Luftwaffe began making head-on attacks the rule. Changes in the Forts followed, adding cheek gun and later chin turrets. By the later years of the war, allied escort fighters made interceptions of the bombers less and less frequent, and the late war Forts had more to worry about from AAA than enemy fighters.
  
The B-17G was the most common model produced, with 8,680 planes built. With a crew of 10 men, thirteen .50 cal machine guns, and a 6000 pound bombload, it had a range of 2000 miles. The G added the Bendix chin turret while otherwise remaining essentially a late model B-17F.

In WarBirds, the B-17 remains the supreme bomber. Given time to line up, the bombsight is extremely accurate from altitudes up to and above 30 thousand feet. Scenarios have seen B-17 formations numbering upwards of 30 planes. With all the gunners firing from such a mass, interceptors could live but a few seconds in the midst of the boxes. With autogunners, the B-17 is hard to attack, but veterans have learned to pull constant g's as they dive in, giving "Otto" a hard time at aiming. Players can also man the guns, and it can be a wild, fun ride taking player-manned buff boxes into battle.
     
Armament:                     
                                   Ammo Load          
 
Primary  : 9 x .50 cal MG's       Varied                   
 
Secondary: None       
 
All guns are gunner operated. Gun positions are: Top, Bottom, Nose, Tail, Right (waist), and Left (waist).