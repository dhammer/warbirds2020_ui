The M4A3E2 Sherman Jumbo is an American tier 6 medium tank. The heavy-armored assault variant of the M4A3(75)W. The vehicle featured additional 38-mm armor plates, enhanced transmission compartment hatch, and a new turret with enhanced armor that was developed on the basis of the T23 turret.

Sloped and thicker armor gives this tank a chance to bounce more shells compared to other Shermans, but the Jumbo is slower and less maneuverable. It has armor rivaling the heaviest tanks around its tier, and even tanks above it like the T29 and Tiger tanks. in addition, it is also smaller than comparable heavy tanks, making it harder for artillery to hit you. 

It possesses a medium's firepower and health pool, combined with a heavy's armor and speed. Due to its thick armor and lack of maneuverability, the Jumbo tends to be more successful as a brawler than as a typical flanking medium tank. Overall, the Jumbo is both very comfortable for beginners and very rewarding for veterans.

Armament:                                           

Primary:  
1 x 75mm main gun OR 76mm main gun.
1 x 0.50 caliber anti-aircraft heavy machine gun on turret
1 x 0.30 caliber coaxial machine gun.
1 x 0.30 caliber bow-mounted machine gun.
1 x Smoke Mortar. 

Ammunition load:
104 x 75mm projectiles.
300 x 12.7mm ammunition.
4,750 x 7.62mm ammunition.
