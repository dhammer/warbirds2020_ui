EAD / GENERAL MOTORS FM-2 "WILDCAT"
-= Full weight =-
7572 lb (3435 kg)

-= Best speed and FTH =-
303 mph at sea level (488 kmh)
315 mph at 3500 ft (507 kmh at 1100 m)
328 mph at 18000 ft (528 kmh at 5500 m)

-= Ca time to 16400 ft (5000 m) at Bst2 =-
6.5 min

-= Service Ceiling =-
34000 ft (10400 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
13.6 sec/lap, 429 ft radius

-= Power ON stall speed at 75% fuel =-
76 mph (122 kmh)

-= Max recommended dive speed (IAS) =-
462 mph (744 kmh)

-= Other notes =-
Rugged fuselage. Improved bomb loadout.


The FM2 arrived later than the F4F's and used the stronger Wright R-1820-56 engine, designed for better performance below 10000 ft. It was produced by the Eastern Aircraft Division of General Motors since the Grumman factories had switched to building the F6F fighter. The FM2, while being surpassed as a dogfighter by most contemporary era planes, was used succesfully in ww2 as a submarine and ship hunter and it's pilots held it in good regard. It was faster at lower altitudes and could carry heavier ordnance than the other Wildcats, while being only slightly heavier than the F4F-3.

"A beer barrel on a roller skate, run through with an ironing board". - This is how one flier described the F4F/FM Wildcat. Outclassed in speed, maneuverability, and range by its primary adversary, the Japanese A6M2 Zero-Sen, the Wildcat nonetheless played an important role in the Pacific war. 
 
Built by Grumman, the F4F was an excellent example of the compromises required of carrier-borne fighters. Small and rugged, but underpowered and heavy for its size, this plane was originally beaten out by the Brewster F2A Buffalo in competition for a new monoplane fighter for the US Navy, but Grumman continued to improve the design and built 54 for the Navy in August 1939.
 
With its ability to withstand damage, Navy pilots were able to use tactics such as the "Thach Weave", developed by US Navy LCDR. Jimmy Thach, to keep this pugnacious warplane fighting. In the Thach Weave, a pair of Wildcats would fly abreast each other and weave back and forth. When an Zero saddled on one Wildcat's tail, his wingman would be in position to fire on the enemy as they crossed in front of him in the weave. When it needed to, the other advantage of the F4F over the Zero was its ability to dive faster, providing its pilots a ready escape should the situation turn bad.
 
Joseph Foss was the highest scoring Wildcat ace with 26 victories while flying from Guadalcanal in 1942. The top scoring FM-2 Ace was Lt. Ralph Elliot with 9 kills.
  
A total of 7316 F4F/FM Wildcats of all types were built for the US Navy and the British Royal Navy, entering service in 1940 and remaining the mainstay of the fleet fighter arm until the introduction of the F6F in 1943. Later, F4F/FM-2's were to serve on almost all of the US Navy's 114 escort carriers.
 
Wildcats had a kill to loss ratio of 6.9 to 1, though many of these kills were bombers and transports.