LOCKHEED P-38G "LIGHTNING"
-= Full weight =-
15680 lb (7112 kg)

-= Best speed and FTH =-
332 mph at sea level (534 kmh)
400 mph at 22000 ft (644 kmh at 6700 m)
404 mph at 26000 ft (650 kmh at 7900 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
6.3 min

-= Service Ceiling =-
38000 ft (11600 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
16.3 sec/lap, 666 ft radius

-= Power ON stall speed at 75% fuel =-
91 mph (146 kmh)

-= Max recommended dive speed (IAS) =-
445 mph (716 kmh)

-= Other notes =-
Excels at high altitudes, should avoid low alt fighting. Compresses above 400 mph. Slow roll rate. Counter rotating propellers.


The P-38G is very similar to the P-38F but 120 lb lighter and can run at 41" of manifold pressure for a longer time with it's V-1710-51/55 engines. It has the same max power of 1225 hp though. An important difference is that the P-38G can use an 8° combat flaps setting which improves the Lightning's turn rate quite a bit.

Developed by Kelly Johnson as a high altitude interceptor and escort fighter and built by Lockheed, the "Fork-Tailed Devil", or P-38, was an abrupt departure in design when its first prototype flew in 1939, and it continued to be an innovative aircraft through its illustrious career. With its twin engines, booms, and tails around a center fuselage, it was the first plane to enter squadron service with tricycle gear, all-metal flush riveted skin, turbo-supercharged engines, and power boosted controls. At the time it entered service it was the fastest and longest ranged fighter in the world.
 
Although it was hindered by technical and logistics problems and was gradually phased out in favor of the P-51 in Europe, the P-38 was an early mainstay in the Mediterranean and considered by many the premier fighter of the Pacific war.
 
America's two leading aces of WW2, Maj. Richard Bong (40 kills) and Maj. Tommy McGuire (38 kills) flew the P-38. Another P-38 claimed the life of Japan's Admiral Yamamoto when it shot down the transport carrying the Japanese commander. P-38's scored the final US air victories of the war when they downed six Ki-84 "Franks" on the day before the final ceasefire.
 
A total of 10,038 P-38's of all types were produced.