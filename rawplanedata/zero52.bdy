MITSUBISHI A6M5a MODEL 52 "REISEN" (ZERO)
-= Full weight =-
6026 lb (2733 kg)

-= Best speed and FTH =-
306 mph at sea level (492 kmh)
333 mph at 7500 ft (536 kmh at 2300 m)
359 mph at 19000 ft (578 kmh at 5800 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
5.9 min

-= Service Ceiling =-
35000 ft (10700 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
12.0 sec/lap, 398 ft radius

-= Power ON stall speed at 75% fuel =-
76 mph (122 kmh)

-= Max recommended dive speed (IAS) =-
435 mph (700 kmh)

-= Other notes =-
Better roll rate than the A6M2. Reduced aileron authority at high speeds. No pilot armor or self sealing fuel tanks.


The model 52 was a further improvement and was faster still than the earlier versions. It fielded the same engine as the A6M3 but had more effective exhaust stacks and the wing cannon bulges removed. The A6M5a kept the shorter wingspan of the model 32 but with rounded wingtips. This had a positive effect regarding induced dragco and wing efficiency. The model 52 also had a somewhat improved design of the flaps and slightly higher max dive speed. The opponents the it faced though were usually significantly faster and could thus dictate the fight against the A6M5a Zero, which had pretty much reached it's limit in terms of development.

Designed in 1937, the Mitsubishi A6M "Zero" or "Zeke" fighter was revolutionary and unbeatable to its Chinese opponents when it was introduced in the summer of 1940. 15 pre-production test models all but ended all Chinese attempts to intercept Japanese bombers over mainland China. Designed as a carrier borne fighter from the start, the A6M was the best in the world during the first years of WW2. Had the Japanese won the early victory in the Pacific as they planned, the Zero may have been enough, but the war dragged on, and the Zero was soon outclassed by better and still better generations of enemy fighters whose numbers Japan could not match.
 
Production of the A6M series totalled approximately 10,449. The A6M2 was the model that first saw action in China, the A6M3 was introduced in July 1941, and the A6M5a saw service starting in Autumn, 1943.
 
Japanese pilots saw the plane as the modern successor to the Samurai's sword, and offensive capability was the main consideration. The Zero's turning ability was almost unmatched by any modern fighter, and it's range was no less than astounding. US planners frequently mistook far-ranging land based Zekes for signs of the presence of a Japanese carrier close by. Two 20mm cannon and two 7.7mm MG's gave it respectable firepower. But the Zero had it's shortcomings. The only front line naval fighter Japan used during most of the war, it's speed and climbing ability were no match for more modern fighters. It's light construction left it vulnerable to damage that would hardly affect the heavier opponents it faced, and even against the F4F, a plane it outperformed, this difference allowed superior team tactics by US fliers to defeat it. By the end of the war, Zero's were being used by Kamikaze pilots in the final defense of the home islands.
 
The top scoring Zero ace was Hiroyoshi Nishizawa. Nicknamed "The Devil" by his squadmates, he was credited with 113 victories although nobody knows an exact count for certain. Some figures place it lower, and some higher. His life came to an end when the transport plane he was flying in as a passenger was intercepted and shot down by American fighters.