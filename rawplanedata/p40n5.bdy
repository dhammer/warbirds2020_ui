CURTISS P-40N-5 "WARHAWK"
-= Full weight =-
8300 lb (3765 kg)

-= Best speed and FTH =-
330 mph at sea level (531 kmh)
377 mph at 10500 ft (607 kmh at 3200 m)
355 mph at 16400 ft (571 kmh at 5000 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
7.6 min

-= Service Ceiling =-
34000 ft (10400 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
14.8 sec/lap, 546 ft radius

-= Power ON stall speed at 75% fuel =-
84 mph (135 kmh)

-= Max recommended dive speed (IAS) =-
485 mph (781 kmh)

-= Other notes =-
Rugged structure.


Many pilots considered the 4x .50 cals of the P-40N-1 to be inadequate armament, and re-equipped their Warhawks with 6x .50 cals again. They also disliked that the P-40N-1 had to be started by a hand-crank by the grundcrew and reinstalled the battery driven engine starter among other equipment. The result was once again an increase in weight. At 157 gal fuel the P-40N-5 was still almost 500 lb lighter than the F though and ~40 lb lighter than the P-40E. The -N versions were the best P-40 versions and also the most produced. The Hawk design had by then unfortunately reached it's peak and started to fall behind newer fighters like the Fw 190's, P-51's, P-38's etc. Nevertheless the Warhawks still held the line at several fronts.

When it entered production in 1940, the P-40 was already outclassed by such fighters as the British Spitfire and German Bf-109. Still, P-40's served in every allied air force and in every theatre during WW2. As it was, the P-40 never caught up with the state-of-the-art, but its toughness and frontline serviceability proved of great value and it served with great success as a tactical fighter-bomber. The prototype first flew in October 1939 and 13,738 P-40's of all types were built.
 
Probably the most famous unit flying P-40's was Gen. Claire Chennault's American Volunteer Group fighting the Japanese in China in 1941 and 42. The P-40D saw the introduction of a new series of Allison engines, resulting in a shorter nose and deeper radiator. With the P-40E, the fuselage guns were discarded and standard armament became six .50 cal MG's.

The top scoring P-40 pilot was Flt. Lt. Clive "Killer" Caldwell of 250 Squadron Raf. An Australian, he flew Tomahawks and Kittyhawks in North Africa and finished the war with 27 kills.