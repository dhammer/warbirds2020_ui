The Ilyushin Il-2 was a ground-attack aircraft (Shturmovik) in the Second World War, produced by the Soviet Union in very large numbers. In combination with its successor, the Ilyushin Il-10, a total of 42,330 were built, making it the single most produced military aircraft design in all of aviation history, as well as one of the most produced piloted aircraft in history. 

The Shturmovik is regarded as the best ground attack aircraft of World War II. It was a prominent aircraft for tank killing with its accuracy in dive bombing and its guns being able to penetrate tanks' thin top armor.

To Il-2 pilots, the aircraft was simply the diminutive "Ilyusha". To the soldiers on the ground, it was the "Hunchback", the "Flying Tank" or the "Flying Infantryman".  

The Il-2 aircraft played a crucial role on the Eastern Front. Joseph Stalin paid the Il-2 a great tribute in his own inimitable manner: when a particular production factory fell behind on its deliveries, Stalin sent an angrily-worded cable to the factory manager, stating "They are as essential to the Red Army as air and bread."
