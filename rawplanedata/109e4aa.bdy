MESSERSCHMITT BF 109E-4 Aa "EMIL"
-= Full weight =-
5759.7 lb (2613 kg)

-= Best speed and FTH =-
310 mph at sea level (499 kmh) [1 min limit]
326 mph at 4000 ft (525 kmh at 1200 m) [1 min limit]
354 mph at 20000 ft (570 kmh at 6100 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
5.7 min

-= Service Ceiling =-
36000 ft (11000 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
13.5 sec/lap, 429 ft radius

-= Power ON stall speed at 75% fuel =-
76 mph (122 kmh)

-= Max recommended dive speed (IAS) =-
470 mph (756 kmh)

-= Other notes =-
Good roll rate. Elevator quickly becomes heavy with increasing speed.


The best known Bf 109E version, the E-4 Aa has the DB 601Aa engine which can produce 1212 hp at 4000 ft for 1 minute stints. 

The "Emil" or E model of the 109 series saw most of its action over the skies of southern England during the Battle of Britain. Originally called on to strike in loosely nit groups high over the bomber formations, the Emil performed very well, nearly bringing the RAF to its knees, but poor decision making on the part of the Axis leadership would lead to its downfall. Squadrons were ordered to take up tight positions close to the bomber formations crossing the English Channel and the 109 lost the advantages of surprise and altitude it had gained over the British Hurricanes and Spitfires.

The Bf-109 in all its variants was produced in larger numbers than any other fighter in history, with over 30,000 built. By itself, that number speaks of the success of this aircraft. Also known as the Me-109 after its creator, Dr. Willy Messerschmitt, the first Bf-109 prototype took flight in 1935 and the final service variant was phased out by the Spanish Air Force in 1967...a career of over 30 years. 
 
Of the types modeled in WarBirds, the 109E began appearing in 1939 and participated in the Battle of Britain. The Bf-109F4 entered service early in 1942 and introduced more drag reduction to the design as well as moving the wing MG's to the nose. The G6 model appeared in December 1943 with the G6/R6 a more heavily armed subvariant of that model, and the Bf-109K4, a high speed high altitude interceptor version, began arriving in October 1944.
 
The Bf-109 incorporated several new advances in structural design and aerodynamics when it was built. 5 years after its first introduction, it remained superior to any opposing fighter, and its many variations kept it abreast of the likes of the Spitfire MkIX and other allied planes throughout the war. However, the 109's success was bolstered in part by the large corps of extremely experienced pilots flying the plane. 
 
The leading fighter ace of all time, Oberst Erich Hartmann, flew the Bf-109 for all of his 352 aerial victories, and there were literally dozens of others with well over 100 kills in 109 variants.