LAVOCHKIN LA-5FN
-= Full weight =-
7323.8 lb (3322 kg)

-= Best speed and FTH =-
366 mph at sea level (589 kmh)
391 mph at 6000 ft (621 kmh at 1500 m)
402 mph at 20000 ft (647 kmh at 6100 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
5.0 min

-= Service Ceiling =-
35000 ft (10700 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
15.7 sec/lap, 685 ft radius

-= Power ON stall speed at 75% fuel =-
96 mph (154 kmh)

-= Max recommended dive speed (IAS) =-
404 mph (650 kmh)

-= Other notes =-
Good roll rate. High wingloading but quite agile.


While the new Shvetsov ASh-82FN engine of the La-5FN only saw moderate improvement from the La-5F's engine, the La-5FN was a big jump in development and saw significant aerodynamical improvements regarding drag, greatly enhancing it's top speed.

The Lavochkin La-5 was developed as a refinement of the LaGG-3 and became one of the most capable fighters of the war and perhaps the single best Russian fighter.

The La-5 and La-7 series aircraft essentially began with the LaGG-1, the underpowered aircraft went through many revisions and modifications to decrease weight, increase power and boost efficiency. That effort was what produced the Lavochkin line of aircraft. 

The La-5 series started when Semyon Lavochkin and Vladimir Gorbunov fitted a LaGG-3 with the new more powerful Shvetsov ASh-82 radial engine in the first half of 1942. Results from a March flight of the prototype proved interesting and worthy of further effort. Russian test pilots gave it rave reviews and work began in earnest in April of that year.

The new aircraft, designated La-5 would undergo more changes and upgrades eventually becoming the La-5F, La-5FN, La-5UTI (trainer version) and would lead to the birth of the La-7 series.

Nearly 10,000 La-5s would be produced before the end of the war, with some seeing service post war in many Eastern-Block nations.