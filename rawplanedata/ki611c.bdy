KAWASAKI KI-61-IC "HIEN" (TONY)
-= Full weight =-
6611 lb (2999 kg)

-= Best speed and FTH =-
316 mph at sea level (509 kmh) [1 min limit]
333 mph at 4600 ft (536 kmh at 1400 m) [1 min limit]
363 mph at 20000 ft (584 kmh at 6100 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
6.2 min

-= Service Ceiling =-
38000 ft (11600 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
12.9 sec/lap, 473 ft radius

-= Power ON stall speed at 75% fuel =-
81 mph (130 kmh)

-= Max recommended dive speed (IAS) =-
528 mph (850 kmh)

-= Other notes =-
Very well designed airframe but outdated engine for it's era. No armored windscreen.


Of all the fighters we've added to WarBirds, none have quite the same star-crossed history of the Ki-61 Hien, or "Tony". The Japanese Army Air Force's first single-seat fighter to enter service after the start of the war, the Ki-61 was intended to replace the now-venerable Ki-43. Designed as an improvement on the Messerschmitt Bf-109e, the Hien used an in-line engine developed in Germany, and in many ways, it resembled the famed German fighter.

The first prototype rolled out of the plant mere days after the attack on Pearl Harbor. However, Kawasaki suffered from production problems from the very start, losing several prototypes and pilots to unexplained accidents in early testing. Materials were scarce, and the Ha-40 engine, the Kawasaki re-design of the Daimler-Benz DB601a, was found to be a fickle powerplant at best.

The fighter was also plagued by extraordinary maintenance problems through its service. Parts were always in short supply, and maintenance crews were poorly trained and short on manpower.

Still, the JAAF had a very formidable fighter when the Ki-61 was operating. Very maneuverable, as nearly all Japanese fighters were, it was also faster (367mph @16k) and much better armored than the other JAAF fighters. Its pilots could afford to be much more aggressive, and against rivals such as the P-39, P-40b and e, and P-38f, it more than held its own.

The Ki-61's very first combat was against a B-25 over Japan during Doolittle's famous raid on Tokyo in April 1942. The Ki-61, which broke off without scoring a kill due to low fuel, was misreported by the B25 crew as a Bf-109, setting off a rash of such reports during the early stages of the Pacific war.

The Ki-61 was next thrown into battle in New Guinea, and the battles over the harsh jungles were a test from which the Hien never recovered. The Ki-61 was not ready for the conditions it faced. It had trouble merely getting to New Guinea, as its pilots were ill-prepared for the long over water flights involved, and getting parts to the remote locations was, it seemed, nearly impossible. Many Ki-61's were simply destroyed on the ground by marauding US 5th Air Force planes, and when the Hien did get in the air, it was frequently outnumbered.

From the debacle of New Guinea, to the losing campaign for the Philippines and back across the Pacific, the Ki-61 always seemed to be fighting a hopeless battle. Veteran pilots were fewer and fewer, and new pilots often had no more than 30 hours total flight time before being commit ed to combat, due to country-wide fuel shortages.

In the final defense of the home islands, the Ki-61 was one of the few fighters able to climb up to attack USAF B-29's at altitude, and then its pilots were forced to save weight by leaving behind equipment including most of their guns... this to attack what was the most heavily armed bomber of the war. Pilots turned to suicidal ramming attacks as the most effective means of stopping the heavy bombers in the last desperate days of the war.