MACCHI C.202 S.III "FOLGORE"
-= Full weight =-
6615.3 lb (3001 kg)

-= Best speed and FTH =-
324 mph at sea level (521 kmh) [1 min limit]
341 mph at 4000 ft (549 kmh at 1200 m) [1 min limit]
371 mph at 20000 ft (597 kmh at 6100 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
6.4 min

-= Service Ceiling =-
36000 ft (11000 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
15.9 sec/lap, 583 ft radius

-= Power ON stall speed at 75% fuel =-
83 mph (134 kmh)

-= Max recommended dive speed (IAS) =-
500 mph (805 kmh)

-= Other notes =-
Good controls at all speeds. Reduced torque effects due to designed difference between the left vs right wing span.


The Macchi C.202 Folgore was built by the Macchi company, a development of their earlier C.200 Saetta, but mounting a more powerful German designed Dailmer-Benz DB 601 engine. Although the C.200 had proven itself as an excellent all-around airframe, it was clear by 1940 that it's puny armament and 319 mph speed was no longer enough to allow it to fight in the front lines. Macchi imported the 1,175hp Daimler-Benz DB 601 from Germany for use with a prototype and started the 202 series.

The results were astonishing. The new C.202 reached 370 mph, even though they had also upgunned the plane with the addition of another pair of 7.7 mm guns in the wings. The new version was immediately put into production as the Folgore (Thunderbolt) using imported engines while Alfa Romeo set up production of the engine under license as the R.A.1000 Monsonie. Engine production was so slow that Macchi was forced to continue building some with the older engines as C.200's (although they included the new wings and guns), but supplies slowly improved and by late 1942 Folgores outnumbered all other fighters in service.

Deliveries started reaching a newly-formed conversion unit, 1st Stormo C.T., in the summer of 1941, and by November they had been moved to the front in Libya. In service the plane proved to be superior to the Curtiss P-40 and Hawker Hurricane, and was considered to be an even match for the Supermarine Spitfire. The plane was loved by the pilots, not only for its performance, but due to its superbly harmonized controls and generally excellent maneuverability and control.

Only two modified versions were built, the C.202AS which included dust filters and other equipment for operations in the desert (AS stands for Africa Settentroniale, North Africa), and the C.202CB (Caccia Bombardiere, or Fighter-Bomber) with underwing hardpoints for two 50, 100 or 160kg bombs, or two 100 litre drop tanks. Macchi produced only 400 of the 1,200 Folgore's eventually built, the rest being supplied by the otherwise underused lines at Breda and Ambrosini.

Looking to improve performance even more, as well as retain commonality with current German engines, the C.202 was later modified with a Dailmer-Benz DB 605 engine to produce the C.205.