CURTISS P-40N-1 "WARHAWK"
-= Full weight =-
7493 lb (3399 kg)

-= Best speed and FTH =-
332 mph at sea level (534 kmh)
379 mph at 10500 ft (610 kmh at 3200 m)
358 mph at 16400 ft (576 kmh at 5000 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
6.6 min

-= Service Ceiling =-
35000 ft (10700 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
13.7 sec/lap, 499 ft radius

-= Power ON stall speed at 75% fuel =-
80 mph (129 kmh)

-= Max recommended dive speed (IAS) =-
485 mph (781 kmh)

-= Other notes =-
Light and agile for a P-40. Rugged structure.


The P-40N-1 saw a huge weight cut as it was evident the P-40's were getting overweight and a lot of non vital equipment was removed in order to make it lighter. It only fielded 4x .50 cals, to the dismay of many pilots. It also only carried 120 gal internal fuel. The engine was the newer Allison V-1710-81 which still produced around 1480 hp, like the P-40E engine, but had it's FTH (Full Throttle Height) at a higher altitude than the P-40E. While it had a weaker firepower than the -E & -F, the result of the weight cut was a faster, better climbing and more agile fighter. The -N versions were the best P-40 versions and also the most produced. The Hawk design had by then unfortunately reached it's peak and started to fall behind newer fighters like the Fw 190's, P-51's, P-38's etc. Nevertheless the Warhawks still held the line at several fronts.

When it entered production in 1940, the P-40 was already outclassed by such fighters as the British Spitfire and German Bf-109. Still, P-40's served in every allied air force and in every theatre during WW2. As it was, the P-40 never caught up with the state-of-the-art, but its toughness and frontline serviceability proved of great value and it served with great success as a tactical fighter-bomber. The prototype first flew in October 1939 and 13,738 P-40's of all types were built.
 
Probably the most famous unit flying P-40's was Gen. Claire Chennault's American Volunteer Group fighting the Japanese in China in 1941 and 42. The P-40D saw the introduction of a new series of Allison engines, resulting in a shorter nose and deeper radiator. With the P-40E, the fuselage guns were discarded and standard armament became six .50 cal MG's.

The top scoring P-40 pilot was Flt. Lt. Clive "Killer" Caldwell of 250 Squadron Raf. An Australian, he flew Tomahawks and Kittyhawks in North Africa and finished the war with 27 kills.