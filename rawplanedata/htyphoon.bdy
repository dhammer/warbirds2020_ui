HAWKER TYPHOON IB - 1944 VERSION
-= Full weight =-
11083 lb (5027 lb)

-= Best speed and FTH =-
366 mph at sea level (589 kmh)
394 mph at 6000 ft (634 kmh at 1800 m)
415 mph at 20000 ft (668 kmh at 6100 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
5.8 min

-= Service Ceiling =-
35000 ft (10700 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
14.5 sec/lap, 548 ft radius

-= Power ON stall speed at 75% fuel =-
85 mph (137 kmh)

-= Max recommended dive speed (IAS) =-
525 mph (845 kmh)

-= Other notes =-
Less drag than the 1942 version. Poor high altitude performance. Slow roll rate. Thick wings, high max angle of attack.


Affectionately dubbed as the Tiffy in RAF slang, the Typhoon's service introduction in mid-1941 was also plagued with problems, and for several months the aircraft faced a doubtful future. However, in 1941 the Luftwaffe brought the formidable Focke-Wulf Fw 190 into service. This saw the Typhoon secure a new role as a low-altitude interceptor as it was the only fighter in the RAF inventory capable of catching the Fw 190 at low altitudes, powered by the powerful Napier Sabre II engine.

Through the support of pilots such as Roland Beamont the Typhoon also established itself in roles such as night-time intruder and a long-range fighter. From late 1942 the Typhoon was equipped with bombs; from late 1943 ground attack rockets were added to the Typhoon's armoury. Using these two weapons, the Typhoon became one of the Second World War's most successful ground-attack aircraft. The Napier Saber IIA was also installed, allowing the Tiffy to use +9 lb/sq.in. of boost. By 1944 there had been several drag reducing changes done to the Typhoon airframe, resulting in a 20 mph speed increase compared to the 1942 production version.


