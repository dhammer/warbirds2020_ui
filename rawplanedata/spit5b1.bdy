SUPERMARINE SPITFIRE Vb
-= Full weight =-
6525 lb (2960 kg)

-= Best speed and FTH =-
323 mph at sea level (520 kmh)
377 mph at 13000 ft (607 kmh at 4000 m)
352 mph at 25000 ft (566 kmh at 7600 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
5.8 min

-= Service Ceiling =-
37000 ft (11300 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
11.7 sec/lap, 456 ft radius

-= Power ON stall speed at 75% fuel =-
78 mph (126 kmh)

-= Max recommended dive speed (IAS) =-
450 mph (724 kmh)

-= Other notes =-
Negative G's engine cutout. Agile with a gentle stall behaviour.


Upgraded with the Merlin 45 engine, the Mk.Vb saw an additional 200 hp increase. This version has had the 8x .303 cals exhanged for 4x .303 cals and 2x 20 mm cannons (60 rpg), which gives it a better punch although the pilot needs to conserve the cannon ammo for sure shots. The Spit Vb is a little heavier than the Mk.II, but the better engine makes up for it regarding power/weight ratio.

In addition to being one of the most beautiful fighters ever built, the Supermarine Spitfire was one of the most robust designs. It had a great wing efficiency due to the eliptical shape of it's wings, resulting in less induced drag effects. With the same airframe that first flew as a prototype in 1936, the Spitfire remained in production throughout WW2, the only Allied fighter to do so. Improvements were mainly to the engine, with the basic shape remarkably able to accommodate the increased size and power of the ever larger Merlins. Combining speed, firepower, and agility into one potent package, the Spit with it's distinctive elliptical wings is always remembered as the plane that won the Battle of Britain... though the Hawker Hurricane carried most of the load.
 
The Spitfire Mk1a was in service at the outbreak of WW2. The Spit V became available in March 1941, and the Spit IX, the most produced variant at 5,665 planes, was introduced in mid 1942.
 
James "Johnny" Johnson was the highest scoring British Spitfire pilot with 38 victories.