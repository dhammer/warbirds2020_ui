HAWKER HURRICANE IIc
-= Full weight =-
7645 lb (3468 kg)

-= Best speed and FTH =-
303 mph at sea level (488 kmh)
347 mph at 13500 ft (558 kmh at 4100 m)
340 mph at 19200 ft (547 kmh at 5850 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
6.5 min

-= Service Ceiling =-
35000 ft (10700 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
13.2 sec/lap, 484 ft radius

-= Power ON stall speed at 75% fuel =-
79 mph (127 kmh)

-= Max recommended dive speed (IAS) =-
390 mph (628 kmh)

-= Other notes =-
Thick wings, high max angle of attack.


The Mk IIc was 312 lb heavier than the IIb, but armed with 4x 20 mm cannons. As such it packed a heavy punch against both fighters and bombers. The Mk II's were all powered by the Merlin XX engine, which had a two speed supercharger capable of approximately 1470 hp at 7000 ft and 1440 hp at 13500 ft. While being significantly heavier than the Mk I's, the Mk II's were still excellent turn fighters but lacked in top speed against contemporary fighters. With top speeds constantly increasing during ww2 the thick wings of the Hurricanes were slowly becoming an achilles heel, and as such the Hawkers were commonly used as fighter bombers, a role in which they performed quite well.

Design of the Hawker Hurricane began in 1933 as Britain looked for its first monoplane fighter. A prototype was flown in November of 1935 and production of the Hurricane began in July 1936 with the first squadrons forming by the end of 1937. All Hurricanes had fabric covered wings until the Autumn of 1939 when the installation of the 1260 hp Merlin XX engine and heavier armament marked the Hurricane II series of variants. The IIa kept the old wings with the newer engine while the IIb had 12 browning machine guns mounted in the new all metal stressed skin wings. The IIc used four 20mm Hispano-Suiza drum fed cannon and the IId was outfitted with a pair of anti-tank cannons. 
 
The memorable Spitfire gets most of the credit for winning the Battle of Britain, but the Hawker Hurricane was the real workhorse of the conflict. The 1715 Hurricanes that served in the battle accounted for four fifths of the enemy aircraft destroyed. The Hurricane expanded it's role into the Mediterranean, North Africa, and the Middle East during 1940, and into the Far East in 1941. It saw air-to-ground action for the most part as later generations of fighters took up the air battle. A total of 14,223 Hurricanes were produced.
 
The top scoring Hurricane ace in WW2 was Squadron Leader Marmaduke Thomas St. John Pattle, a South African who served in North Africa and later in Greece. He had 35 victories while flying the Hurricane. Some estimates give him 50 kills altogether which would also make him the highest scoring RAF ace of the war.