POLIKARPOV I-16 "ISHAK"
-= Full weight =-
4215 lb (1912 kg)

-= Best speed and FTH =-
286 mph at sea level (460 kmh)
296 mph at 3000 ft (476 kmh at 900 m)
305 mph at 15000 ft (491 kmh at 4600 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
5.6 min

-= Service Ceiling =-
32000 ft (9750 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
12.7 sec/lap, 359 ft radius

-= Power ON stall speed at 75% fuel =-
72 mph (116 kmh)

-= Max recommended dive speed (IAS) =-
475 mph (764 kmh)

-= Other notes =-
Negative G's engine cutout.


The Polikarpov I-16 Ishak (Donkey) Type 24 fighter was a light fighter at only 4215 lb and powered by a 1115 hp Shvetsov engine. It had a quite decent power/weight ratio for fighters of the 1939-1940 era. It's stubby feature meant a larger drag, and the top speed was only slightly better than that of a B-239 Buffalo. The climb rate was decent, slightly hampered by the two bladed prop, but the all-around vision of the pilot was good. The Ishak only fielded an armament of 4x 7.62 mm machineguns. It also lacked flaps, as the Russian pilots deemed them unnecessary and mailfunctioning and more often than not had the groundcrew fix the flaps rigidly shut. It was a very agile fighter though that could both turn and roll quite well.

The I-16 was a Soviet fighter aircraft of revolutionary design; it was the world's first low-wing cantilever monoplane fighter with retractable landing gear to attain operational status and as such "introduced a new vogue in fighter design. It was introduced in the mid-1930s and formed the backbone of the Soviet Air Force at the beginning of World War II. Polikarpov I-16's also fought on the Republican side in the Spanish civil and made combat entry in November 1936. It outperformed the Nationalist Heinkel He 51's and Arado Ar 68 biplanes, and didn't meet it's match until the introduction of the Bf 109. The plane was nicknamed "Rata" (Rat) by the Nationalists and "Mosca" (Fly) by the Republicans.

