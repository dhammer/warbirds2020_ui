GRUMMAN F6F-5 "HELLCAT"
-= Full weight =-
12415 lb (5631 kg)

-= Best speed and FTH =-
351 mph at sea level (565 kmh)
397 mph at 14600 ft (639 kmh at 4450 m)
405 mph at 19500 ft (652 kmh at 5950 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
6.5 min

-= Service Ceiling =-
38000 ft (11600 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
16.0 sec/lap, 620 ft radius

-= Power ON stall speed at 75% fuel =-
90 mph (145 kmh)

-= Max recommended dive speed (IAS) =-
495 mph (797 kmh)

-= Other notes =-
Rugged fuselage. 14 minutes water injection capacity per sortie.


A common misconception is the rumour that even though they used the same engine, the F6F Hellcat had an inferior top speed compared to the F4U Corsair. This misconception was derived from the F6F's having an erratic speed gauge instrument, which also explains why it's stall speed was stated as unrealistically low, lower than for an A6M Zero, in the F6F Pilot Handbooks. The faulty speed gauge system of the F6F was discovered when Vaught donated a F4U Corsair to Grumman to help finding the cause of the F6F's presumably inferior top speed. In the trials it was discovered that the two planes were just as fast, keeping up side by side at full throttle. Even though later Japanese fighters like the J2M and Ki-84 etc could outperform the F6F-5, the Hellcat was a great fighter against the A6M Zero, which it was specifically designed to counter.

The Grumman F6F was developed as a backup to the Vought F4U, should that design encounter any setbacks.  It was a precaution that paid dividends as the F6F was to become the Navy's workhorse fighter in WWII.  With over 12,200 F6Fs produced, more than 5000 Japanese aircraft destroyed, and an astounding 19:1 kill to loss ratio, the F6F proved to be one of the most redoubtable fighters of WWII.
 
The F6F was built in two major variants, the F6F-3 and the F6F-5. The F6F-3 entered service at the beginning of 1943. 4,423 F6F-3s were built before that variant was replaced by the F6F-5 in mid 1944.  The F6F-5 would become the major variant with 7870 produced.  It incorporated a number of minor improvements, but overall, little was changed in the design.  The canopy was redesigned, rocket rails were added, more armor was added, the tail was structurally reinforced, and water injection was added.
 
The U.S. Navy's highest scoring ace and the third leading American ace, Commander David McCampbell, scored all 34 of his victories while flying the Hellcat.  This was the most victories by any American pilot in a single tour of duty.  He was also the only American to down 5 or more planes in a single day on two separate occasions, including 9 victories in a single sortie, a figure unmatched by any other Allied pilot.
 
The F6F was a good design, although not exceedingly superior in any one area. However, it was a plane of few vices and it's reliability and durability were not insignificant factors in it's success.