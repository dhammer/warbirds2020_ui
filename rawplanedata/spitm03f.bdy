SUPERMARINE SPITFIRE I
-= Full weight =-
6050 lb (2744 kg)

-= Best speed and FTH =-
306 mph at sea level (492 kmh)
344 mph at 9000 ft (554 kmh at 2700 m)
317 mph at 25000 ft (510 kmh at 7600 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
5.7 min

-= Service Ceiling =-
34000 ft (10400 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
11.5 sec/lap, 435 ft radius

-= Power ON stall speed at 75% fuel =-
75 mph (121 kmh)

-= Max recommended dive speed (IAS) =-
450 mph (724 kmh)

-= Other notes =-
Negative G's engine cutout. Agile with a gentle stall behaviour.


The Mk.Ia was used during the invasion of France, pre Battle of Britain. It used 87 octane fuel and as such was limited to +6.25 lbs of boost. This hampered it's top speed and sustained turn performance compared to the Mk.I. The Mk.Ia and Mk.I were otherwise very similar.

In addition to being one of the most beautiful fighters ever built, the Supermarine Spitfire was one of the most robust designs. It had a great wing efficiency due to the eliptical shape of it's wings, resulting in less induced drag effects. With the same airframe that first flew as a prototype in 1936, the Spitfire remained in production throughout WW2, the only Allied fighter to do so. Improvements were mainly to the engine, with the basic shape remarkably able to accommodate the increased size and power of the ever larger Merlins. Combining speed, firepower, and agility into one potent package, the Spit with it's distinctive elliptical wings is always remembered as the plane that won the Battle of Britain... though the Hawker Hurricane carried most of the load.
 
The Spitfire Mk1a was in service at the outbreak of WW2. The Spit V became available in March 1941, and the Spit IX, the most produced variant at 5,665 planes, was introduced in mid 1942.
 
James "Johnny" Johnson was the highest scoring British Spitfire pilot with 38 victories.