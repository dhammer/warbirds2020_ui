HAWKER HURRICANE IId
-= Full weight =-
8121 lb (3684 kg)

-= Best speed and FTH =-
299 mph at sea level (481 kmh)
343 mph at 13500 ft (552 kmh at 4100 m)
335 mph at 19200 ft (539 kmh at 5850 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
7.1 min

-= Service Ceiling =-
34000 ft (10400 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
14.4 sec/lap, 524 ft radius

-= Power ON stall speed at 75% fuel =-
82 mph (132 kmh)

-= Max recommended dive speed (IAS) =-
390 mph (628 kmh)

-= Other notes =-
Thick wings, high max angle of attack. Extra thick engine and pilot armor.


Hurricane Mk IID was a Hurricane Mk IIB conversion armed with two AT cannons in a pod under each wing and a single Browning machine gun in each wing loaded with tracers for aiming purposes. The first aircraft flew on 18 September 1941 and deliveries started in 1942. Serial built aircraft had additional armour for the pilot, radiator and engine. The weight of guns and armour protection marginally impacted the aircraft's performance. These Hurricanes were nicknamed "Flying Can Openers", perhaps a play on the No. 6 Squadron's logo which flew the Hurricane starting in 1941.

Design of the Hawker Hurricane began in 1933 as Britain looked for its first monoplane fighter. A prototype was flown in November of 1935 and production of the Hurricane began in July 1936 with the first squadrons forming by the end of 1937. All Hurricanes had fabric covered wings until the Autumn of 1939 when the installation of the 1260 hp Merlin XX engine and heavier armament marked the Hurricane II series of variants. The IIa kept the old wings with the newer engine while the IIb had 12 browning machine guns mounted in the new all metal stressed skin wings. The IIc used four 20mm Hispano-Suiza drum fed cannon and the IId was outfitted with a pair of anti-tank cannons.

The memorable Spitfire gets most of the credit for winning the Battle of Britain, but the Hawker Hurricane was the real workhorse of the conflict. The 1715 Hurricanes that served in the battle accounted for four fifths of the enemy aircraft destroyed. The Hurricane expanded it's role into the Mediterranean, North Africa, and the Middle East during 1940, and into the Far East in 1941. It saw air-to-ground action for the most part as later generations of fighters took up the air battle. A total of 14,223 Hurricanes were produced.
 
The top scoring Hurricane ace in WW2 was Squadron Leader Marmaduke Thomas St. John Pattle, a South African who served in North Africa and later in Greece. He had 35 victories while flying the Hurricane. Some estimates give him 50 kills altogether which would also make him the highest scoring RAF ace of the war.

