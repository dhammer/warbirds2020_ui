Laid down and launched as a battleship, work on the Kaga (named for a city in Japan) was stopped in accordance with the terms of the Washington Naval Treaty of 1922. The following year, work to finish her as an aircraft carrier began, and Kaga was completed on March 31, 1928. 

One of the six carriers in Vice Admiral Chuichi Nagumo's First Air Fleet sent against Pearl Harbor on December 7, 1941, Kaga later took part in operations against the Dutch East Indies and New Guinea. 

One of four carriers in Admiral Nagumo's fleet at the Battle of Midway, Kaga was sunk by dive-bombers from USS Enterprise at about 10:26 on June 4, 1942, with the loss of 800 crew.
