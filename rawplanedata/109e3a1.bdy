MESSERSCHMITT BF 109E-3 "EMIL"
-= Full weight =-
5749.7 lb (2608 kg)

-= Best speed and FTH =-
305 mph at sea level (491 kmh) [1 min limit]
326 mph at 5400 ft (525 kmh at 1650 m) [1 min limit]
342 mph at 15000 ft (550 kmh at 4600 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
5.9 min

-= Service Ceiling =-
35000 ft (10700 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
13.7 sec/lap, 425 ft radius

-= Power ON stall speed at 75% fuel =-
76 mph (122 kmh)

-= Max recommended dive speed (IAS) =-
470 mph (756 kmh)

-= Other notes =-
Good roll rate. Elevator quickly becomes heavy with increasing speed.


The E-1 is lightly armed with 4x 7.92 mm MG's and has a max output of 1134 hp at 4100 ft for 1 minute stints with it's DB 601 engine.

The "Emil" or E model of the 109 series saw most of its action over the skies of southern England during the Battle of Britain. Originally called on to strike in loosely nit groups high over the bomber formations, the Emil performed very well, nearly bringing the RAF to its knees, but poor decision making on the part of the Axis leadership would lead to its downfall. Squadrons were ordered to take up tight positions close to the bomber formations crossing the English Channel and the 109 lost the advantages of surprise and altitude it had gained over the British Hurricanes and Spitfires.

The Bf-109 in all its variants was produced in larger numbers than any other fighter in history, with over 30,000 built. By itself, that number speaks of the success of this aircraft. Also known as the Me-109 after its creator, Dr. Willy Messerschmitt, the first Bf-109 prototype took flight in 1935 and the final service variant was phased out by the Spanish Air Force in 1967...a career of over 30 years. 
 
Of the types modeled in WarBirds, the 109E began appearing in 1939 and participated in the Battle of Britain. The Bf-109F4 entered service early in 1942 and introduced more drag reduction to the design as well as moving the wing MG's to the nose. The G6 model appeared in December 1943 with the G6/R6 a more heavily armed subvariant of that model, and the Bf-109K4, a high speed high altitude interceptor version, began arriving in October 1944.
 
The Bf-109 incorporated several new advances in structural design and aerodynamics when it was built. 5 years after its first introduction, it remained superior to any opposing fighter, and its many variations kept it abreast of the likes of the Spitfire MkIX and other allied planes throughout the war. However, the 109's success was bolstered in part by the large corps of extremely experienced pilots flying the plane. 
 
The leading fighter ace of all time, Oberst Erich Hartmann, flew the Bf-109 for all of his 352 aerial victories, and there were literally dozens of others with well over 100 kills in 109 variants.