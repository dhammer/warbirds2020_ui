DE HAVILLAND DH.98 "MOSQUITO" FB.VI
-= Full weight =-
20400 lb (9253 kg)

-= Best speed and FTH =-
369 mph at sea level (594 kmh)
385 mph at 7000 ft (620 kmh at 2100 m)
381 mph at 13000 ft (613 kmh at 4000 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
8.7 min

-= Service Ceiling =-
29000 ft (8800 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
16.6 sec/lap, 783 ft radius

-= Power ON stall speed at 75% fuel =-
100 mph (161 kmh)

-= Max recommended dive speed (IAS) =-
450 mph (724 kmh)

-= Other notes =-
Good controls at all speeds. Slow roll rate. 


The Mk VI version was a further improvement of the DH.98 series that fielded the newer Rolls Royce Merlin 25 engines. While having slightly less horsepowers than previous Mosquito versions at base power, the new use of 150 grade fuel for +25 lbs boost below 13000 ft resulted in approximately a 23 mph higher top speed at low altitude. The Mosquito VI was a sleek, fast tactical fighter/bomber often able to get in to it's target and out unscathed.

The "Wooden Wonder" was rejected as a concept by Britain's Air Marshals, and had to be designed and built in an English manor house and test flown off an adjoining farm field. It became possibly the most successful and certainly most versatile British aircraft of WW2.

It served as Photo-recon, day fighter, night fighter, bomber, mine layer, torpedo bomber, tank buster, pathfinder, and submarine hunter. What's left?

Sir Geoffrey de Havilland proposed his wooden speed bomber in in 1938. When he was turned down, he said "We'll do it anyway". With the encouragement of Air Chief Marshal Sir Wilfred Freeman, de Havilland set up shop in Salisbury Hall and set to work. The prototype flew in November 1940 and went into production.

Wood was not a scarce commodity like the metals used in other planes, and much of the skilled workforce came from otherwise underemployed furniture craftsmen...making the Mosquito doubly valued, as much for its manufacturing efficiency as its performance.

7,781 Mosquitos of all types were eventually built. At near 400 mph level speed, it had the ability to penetrate deep into enemy territory while avoiding interception. In 1943, an 11am address in Berlin by Goering in which it was boasted that no enemy bomber would ever reach Berlin unscathed was interrupted by three Mosquitos from 105 squadron who dropped their bombs on the city and returned safely to English soil. At 4pm that same day, a similar address by Goebbels was interrupted in the same manner. It was an outstanding success, almost stage-managed by Goering himself, that gave the Mosquito a moral advantage it never relinquished.