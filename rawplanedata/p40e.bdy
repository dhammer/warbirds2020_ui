CURTISS P-40E "KITTYHAWK"
-= Full weight =-
8342.4 lb (3784 kg)

-= Best speed and FTH =-
339 mph at sea level (546 kmh)
352 mph at 3500 ft (566 kmh at 1100 m)
349 mph at 12000 ft (562 kmh at 3700 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
7.5 min

-= Service Ceiling =-
30000 ft (9100 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
14.8 sec/lap, 542 ft radius

-= Power ON stall speed at 75% fuel =-
84 mph (135 kmh)

-= Max recommended dive speed (IAS) =-
480 mph (772 kmh)

-= Other notes =-
Rugged structure.


The P-40E was fitted with the newer V-1710-39 engine which, once cleared for 56" Hg WEP, provided 1470 hp at sea level compared to the P-40B's 1040 hp. The -E was faster than the -B up to 15000 ft. The Curtiss engineers kept loading their P-40's with extra equipment for each version, and the P-40E was even heavier than it's predecessor. The armament consisted of 6x .50 cals which could pack a strong punch.

When it entered production in 1940, the P-40 was already outclassed by such fighters as the British Spitfire and German Bf-109. Still, P-40's served in every allied air force and in every theatre during WW2. As it was, the P-40 never caught up with the state-of-the-art, but its toughness and frontline serviceability proved of great value and it served with great success as a tactical fighter-bomber. The prototype first flew in October 1939 and 13,738 P-40's of all types were built.
 
Probably the most famous unit flying P-40's was Gen. Claire Chennault's American Volunteer Group fighting the Japanese in China in 1941 and 42. The P-40D saw the introduction of a new series of Allison engines, resulting in a shorter nose and deeper radiator. With the P-40E, the fuselage guns were discarded and standard armament became six .50 cal MG's.

The top scoring P-40 pilot was Flt. Lt. Clive "Killer" Caldwell of 250 Squadron Raf. An Australian, he flew Tomahawks and Kittyhawks in North Africa and finished the war with 27 kills.