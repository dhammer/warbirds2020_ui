Sd.Kfz. 7/2 armed with a 3.7 cm FlaK 36 anti-aircraft gun (in 1944 switched to 3.7 cm FlaK 43). Again, both open and armored cab variants existed. About 1000 produced by the end of January 1945.  The Sd.Kfz. 7/2 saw extensive use in the North African Campaign where their tracks allowed them to drive through the desert sands far more effectively than trucks. Often columns carrying troops or POW's would include at least two half tracks with one generally riding point in order to make a path through the sands that the trucks could follow.

Armament:                                           
a prime mover for smaller towed guns, such as the 2 cm FlaK 30 or the 3.7 cm PaK 36 anti-tank gun, capable of carrying up to 8 soldiers in addition.

Ammunition load: