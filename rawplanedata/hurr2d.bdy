Design of the Hawker Hurricane began in 1933 as Britain looked for its first monoplane fighter. A prototype was flown in November of 1935 and production of the Hurricane began in July 1936 with the first squadrons forming by the end of 1937. All Hurricanes had fabric covered wings until the Autumn of 1939 when the installation of the 1260 hp Merlin XX engine and heavier armament marked the Hurricane II series of variants. The IIa kept the old wings with the newer engine while the IIb had 12 browning machine guns mounted in the new all metal stressed skin wings. The IIc used four 20mm Hispano-Suiza drum fed 
cannon and the IId was outfitted with a pair of 40mm anti-tank cannon. 

The memorable Spitfire gets most of the credit for winning the Battle of Britain, but the Hawker Hurricane was the real workhorse of the conflict. The 1715 Hurricanes that served in the battle accounted for four fifths of the enemy aircraft destroyed. The Hurricane expanded it's role into the Mediterranean, North Africa, and the Middle East during 1940, and into the Far East in 1941. It saw air-to-ground action for the most part as later generations of fighters took up the air battle. A total of 14,223 Hurricanes were produced.
 
The top scoring Hurricane ace in WW2 was Squadron Leader Marmaduke Thomas St. John Pattle, a South African who served in North Africa and later in Greece. He had 35 victories while flying the Hurricane. Some estimates give him 50 kills altogether which would also make him the highest scoring RAF ace of the war.

Hurricane Mk IID 
Hurricane Mk IIB conversion armed with two 40 mm (1.57 in) AT cannons in a pod under each wing and a single Browning machine gun in each wing loaded with tracers for aiming purposes. The first aircraft flew on 18 September 1941 and deliveries started in 1942. Serial built aircraft had additional armour for the pilot, radiator and engine, and were armed with a Rolls-Royce gun with 12 rounds, later changed to the 40 mm (1.57 in) Vickers S gun with 15 rounds. The outer wing attachments were strengthened so that 4G could be pulled at a weight of 8,540 lb (3,874 kg).[77] The weight of guns and armour protection marginally impacted the aircraft's performance. These Hurricanes were nicknamed "Flying Can Openers", perhaps a play on the No. 6 Squadron's logo which flew the Hurricane starting in 1941.


Armament:                     
                                   Ammo Load          
 
Primary  : 2 x .303 MG's           350 rpg            
 
Secondary: 2 x 40mm Vickers S gun   15 rpg            
 
All guns are wing mounted.
 
 
The "Ammo Load" is based on the historic normal operational ammo load for the plane/weapon(s) in question.