MESSERSCHMITT BF 109G-6 "GUSTAV"
-= Full weight =-
6911.5 lb (3135 kg)

-= Best speed and FTH =-
344 mph at sea level (554 kmh) [1 min limit]
373 mph at 7000 ft (600 kmh at 2100 m) [1 min limit]
407 mph at 23000 ft (655 kmh at 7000 m) [1 min limit]

-= Ca time to 16400 ft (5000 m) at Bst1 =-
4.8 min

-= Service Ceiling =-
39000 ft (11900 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
15.2 sec/lap, 536 ft radius

-= Power ON stall speed at 75% fuel =-
84 mph (135 kmh)

-= Max recommended dive speed (IAS) =-
470 mph (756 kmh)

-= Other notes =-
Good roll rate. Elevator quickly becomes heavy with increasing speed.


The G-6 has the DB 605A-1 engine like the G-2, but is cleared for use of 1.42 ata with a max output of 1529 hp at 6800 ft at 1 minute stints. It has improved armament with 2x 13 mm MG's replacing the 7.92 MG's and packs a better punch. It's a bit heavier than the F's though and slightly worse in turn fight at low alts, but is a good high alt performer.

The Bf-109 in all its variants was produced in larger numbers than any other fighter in history, with over 30,000 built. By itself, that number speaks of the success of this aircraft. Also known as the Me-109 after its creator, Dr. Willy Messerschmitt, the first Bf-109 prototype took flight in 1935 and the final service variant was phased out by the Spanish Air Force in 1967, a career of over 30 years. 
 
Of the types modeled in WarBirds, the 109E began appearing in 1939 and participated in the Battle of Britain. The Bf-109F4 entered service early in 1942 and introduced more drag reduction to the design as well as moving the wing MG's to the nose. The G6 model appeared in December 1943 with the G6/R6 a more heavily armed subvariant of that model, and the Bf-109K4, a high speed high altitude interceptor version, began arriving in October 1944.
 
The Bf-109 incorporated several new advances in structural design and aerodynamics when it was built. 5 years after its first introduction, it remained superior to any opposing fighter, and its many variations kept it abreast of the likes of the Spitfire MkIX and other allied planes throughout the war. However, the 109's success was bolstered in part by the large corps of extremely experienced pilots flying the plane. 
 
The leading fighter ace of all time, Oberst Erich Hartmann, flew the Bf-109 for all of his 352 aerial victories, and there were literally dozens of others with well over 100 kills in 109 variants.