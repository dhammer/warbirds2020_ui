The Half-Track M3 soon appeared in 1941 and began to supplement the M2 types in frontline service. The M3 differed somewhat from the preceding design in that it showcased a lengthened hull. Original forms featured a pedestal-mounted 0.50 caliber machine gun at center for self-defense but this was later replaced by a more traditional "pulpit" style assembly on future production models. 

Beyond the M3 came the "M5" which differed little more than in the manufacturing process used. The "M9" was a related half-track armored car design.

The M3 was then spawned into a myriad of roles from the base personnel carrier. One of the most fearsome was the "Quad-50" anti-aircraft platform which saw a battery of 4 x 0.50 (12.7mm) caliber Browning heavy machine guns mounted atop a turning pedestal. 

This formidable array proved exceptional in the low-level air defense role and could be turned on unfortunate enemy infantry in a pinch as well. Other variants of the M3 series became specialized gun carriers mounting weaponry from 57mm to 105mm self-propelled guns. Mortar carriers, armored ambulances and engineering vehicles were also produced.

Armament:
1 x 0.50 caliber heavy machine gun OR 1 x 0.30 caliber M1919A4 medium machine gun. Also any personal passenger weapons could come into play.
Other mission specific variants included:
1 x 81mm mortar
1 x 57mm anti-tank gun
1 x 75mm field gun
1 x 105mm howitzer

2 or 4 x 0.50 caliber heavy machine guns (AA)
2 or 4 x 20mm Bofors cannons
2 or 4 x 40mm Bofors cannons

Ammunition:
 700 x 12.7mm ammunition OR 7,750 x 7.62mm ammunition
