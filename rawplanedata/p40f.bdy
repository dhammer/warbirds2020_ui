CURTISS P-40F "WARHAWK"
-= Full weight =-
8779 lb (3982 kg)

-= Best speed and FTH =-
335 mph at sea level (539 kmh)
383 mph at 13500 ft (616 kmh at 4100 m)
374 mph at 19200 ft (602 kmh at 5850 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
7.5 min

-= Service Ceiling =-
34000 ft (10400 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
16.2 sec/lap, 590 ft radius

-= Power ON stall speed at 75% fuel =-
87 mph ( kmh)

-= Max recommended dive speed (IAS) =-
480 mph (772 kmh)

-= Other notes =-
Rugged structure.


The P-40F was different from the other P-40's in that it had the single stage Allison engine replaced with a Merlin XX engine, same as the Hurricane II, and was license built by the U.S. as Packard V-1650-1. This was a two speed supercharger engine and as such greatly improved the P-40F's high altitude performance compared to the other Hawks. The drawback with the P-40F though was the U.S. engineers tendency to keep overloading their fighters with extra equipment, meaning this bird was even heavier than the earlier versions.

When it entered production in 1940, the P-40 was already outclassed by such fighters as the British Spitfire and German Bf-109. Still, P-40's served in every allied air force and in every theatre during WW2. As it was, the P-40 never caught up with the state-of-the-art, but its toughness and rugged construction made it easy to service on frontlines, and proved of great value as a successful tactical fighter-bomber. The prototype first flew in October 1939 and 13,738 P-40's of all types were built.
 
Probably the most famous unit flying P-40's was Gen. Claire Chennault's American Volunteer Group (AVG) fighting the Japanese in China in 1941 and 42. The P-40D saw the introduction of a new series of Allison engines, resulting in a shorter nose and deeper radiator. With the P-40E, the fuselage guns were discarded and standard armament became six .50 cal MG's.

The top scoring P-40 pilot was Flt. Lt. Clive "Killer" Caldwell of 250 Squadron Raf. An Australian, he flew Tomahawks and Kittyhawks in North Africa and finished the war with 27 kills.