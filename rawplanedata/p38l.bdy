LOCKHEED P-38L "LIGHTNING"
-= Full weight =-
18055 lb (8190 kg)

-= Best speed and FTH =-
344 mph at sea level (554 kmh)
417 mph at 26000 ft (671 kmh at 7900 m)
412 mph at 29000 ft (663 kmh at 8800 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
5.7 min

-= Service Ceiling =-
39000 ft (11900 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
17.1 sec/lap, 762 ft radius

-= Power ON stall speed at 75% fuel =-
98 mph (158 kmh)

-= Max recommended dive speed (IAS) =-
445 mph (716 kmh)

-= Other notes =-
Excels at high altitudes, should avoid low alt fighting. Compresses above 420 mph, 20 mph better than previous P-38's due to dive flaps (these are simulated automatic in Warbirds). Improved roll rate at high speeds. Counter rotating propellers.


While being the heaviest of the Lightnings, the P-38L has several improvements. The most significant one is the power boosted ailerons. This gives the P-38L a much needed increase to the roll rate at high speeds, and also a slight improvement at lower speeds. The special designed dive recovery flaps allows the -L to dive 20 mph faster before compression occurs. The L-version's V-1710-111/113 engines are very similar to the P-38J engines and also able to produce 1570 hp. The ordnance is even more enhanced with the P-38L being able to carry 2x 2000 lb bombs, making it very effective for jabo missions.

Developed by Kelly Johnson as a high altitude interceptor and escort fighter and built by Lockheed, the "Fork-Tailed Devil", or P-38, was an abrupt departure in design when its first prototype flew in 1939, and it continued to be an innovative aircraft through its illustrious career. With its twin engines, booms, and tails around a center fuselage, it was the first plane to enter squadron service with tricycle gear, all-metal flush riveted skin, turbo-supercharged engines, and power boosted controls. At the time it entered service it was the fastest and longest ranged fighter in the world.
 
Although it was hindered by technical and logistics problems and was gradually phased out in favor of the P-51 in Europe, the P-38 was an early mainstay in the Mediterranean and considered by many the premier fighter of the Pacific war.
 
America's two leading aces of WW2, Maj. Richard Bong (40 kills) and Maj. Tommy McGuire (38 kills) flew the P-38. Another P-38 claimed the life of Japan's Admiral Yamamoto when it shot down the transport carrying the Japanese commander. P-38's scored the final US air victories of the war when they downed six Ki-84 "Franks" on the day before the final ceasefire.
 
A total of 10,038 P-38's of all types were produced.