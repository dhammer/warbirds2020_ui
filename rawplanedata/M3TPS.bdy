T4 Scout/AT

The T4 Scout/AT was a variant of the T48 57 mm Gun Motor Carriage self-propelled anti-tank gun, used by the Soviet Union. The original design incorporated a 57 mm gun M1, a US production of the British Ordnance QF 6 pounder, mounted on an M3 Half-track.

A total of 962 vehicles were produced from 1942 to 1943 for Great Britain, intended for use in the Western Desert   By the time they were produced the campaign was over. As a result, the British transferred 650 half-tracks to the Soviet Union under the Soviet Aid Program. 

The Soviets called it the SU-57 (Samokhodnaya ustanovka 57); under this designation it served in Operation Bagration and other fighting on the Eastern Front during World War II.
