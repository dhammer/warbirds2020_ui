SUPERMARINE SPITFIRE LF IXe
-= Full weight =-
7597.7 lb (3446 kg)

-= Best speed and FTH =-
337 mph at sea level (542 kmh)
372 mph at 9400 ft (599 kmh at 2900 m)
406 mph at 21000 ft (653 kmh at 6400 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
5.2 min

-= Service Ceiling =-
41000 ft (12500 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
12.9 sec/lap, 527 ft radius

-= Power ON stall speed at 75% fuel =-
84 mph (135 kmh)

-= Max recommended dive speed (IAS) =-
450 mph (724 kmh)

-= Other notes =-
Agile with a gentle stall behaviour.


The LF IXe was a great improvement to the Supermarine series, and by many considered as one of the best fighters of it's era. The Merlin 66 engine was a work of art and could produce 1742 hp at 9400 ft, and was impressive for a mid 1943 inline engine. The LF IXe could reach around 375 mph at 9400 ft and 406 mph at 21000 ft, which was equal to the Fw 190A-8, altough the Fw 190A-8 was faster at sea level. The LF IXe also had an improved armament with 2x .50 cals replacing the 4x .303 cals of the IXa.

In addition to being one of the most beautiful fighters ever built, the Supermarine Spitfire was one of the most robust designs. It had a great wing efficiency due to the eliptical shape of it's wings, resulting in less induced drag effects. With the same airframe that first flew as a prototype in 1936, the Spitfire remained in production throughout WW2, the only Allied fighter to do so. Improvements were mainly to the engine, with the basic shape remarkably able to accommodate the increased size and power of the ever larger Merlins. Combining speed, firepower, and agility into one potent package, the Spit with it's distinctive elliptical wings is always remembered as the plane that won the Battle of Britain... though the Hawker Hurricane carried most of the load.
 
The Spitfire Mk1a was in service at the outbreak of WW2. The Spit V became available in March 1941, and the Spit IX, the most produced variant at 5,665 planes, was introduced in mid 1942.
 
James "Johnny" Johnson was the highest scoring British Spitfire pilot with 38 victories.