At the start of the Pacific war, the Japanese had perhaps the world's most advanced bomber in the G4M1 "Betty". It boasted extraordinary range and speed for a bomber, and gave the Japanese Navy the ability to strike at targets across the long expanse of the Pacific ocean.

Also known as the Navy Attack Bomber type 1, development began in 1937 with Mitsubishi. A prototype was flown in October 1939, and was able to obtain a top level speed of 276 mph and a range in excess of 3000 nautical miles.

Production began in 1940, with 2,446 planes of both the G4M1 and later G4M2 being built.

The Betty had some drawbacks, chief among them the lack of armor or self-sealing fuel tanks, these having been left out to obtain the speed and range called for. When the tide of the war in the Pacific changed, throwing the Japanese on the defensive, the great range became less important than its vulnerability to enemy air power. With its tendency to burst into flames after a few hits, it earned the nickname "One shot lighter" by US aircrews.

In WarBirds, the Betty's speed will no doubt be a factor while its great range will not, given the nature of the game. It does fill a gap in the planeset, and will likely see alot of use in scenarios.