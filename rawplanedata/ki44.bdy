NAKAJIMA KI-44-IIB/C "SHOKI" (TOJO)
-= Full weight =-
6093.6 lb (2764 kg)

-= Best speed and FTH =-
333 mph at sea level (536 kmh)
362 mph at 7000 ft (583 kmh at 2100 m)
383 mph at 17600 ft (616 kmh at 5400 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
4.3 min

-= Service Ceiling =-
37000 ft (11300 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
14.3 sec/lap, 621 ft radius

-= Power ON stall speed at 75% fuel =-
92 mph (148 kmh)

-= Max recommended dive speed (IAS) =-
480 mph (772 kmh)

-= Other notes =-
No armored windscreen. Relatively fragile.


The KI-44 Shoki was very different from traditional Japanese fighter design in that it was built for speed, not turning performance. It's a large nosed fighter with small and very thin wings, fielding the strong Nakajma Ha-103 radial engine capable of nearly 1600 hp at 2100 m (~7000 ft). It could match a P-38G in top speed up to 5200 m (~17000 ft). The most impressive performance of this fighter is it's climb rate though, outmatching most fighters of WW2. It's armament is decent with 4x 12.7 mm machine guns and the short wingspan gives it a good roll rate. In contrast to the Ki-43, the Ki-44 is a very good diver and can sustain high speeds. It has no armored windscreen so machine gun fire from bombers can be dangerous. The thin small wings of the Ki-44 hampers it's turn radius at high speeds, but it's relatively light weight and strong engine still ensures that this bird is a decent turn fighter at slower speeds, especially in a spiral climbing turn. The low drag butterfly flaps also help improve it's turning performance. Overall the Ki-44-II Shoki is a quite competitive fighter, and had the Japanese Army pilots been better trained then it would have been a much more dangerous opponent for the U.S. pilots.