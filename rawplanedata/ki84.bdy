NAKAJIMA KI-84-IA "HAYATE" (FRANK)
-= Full weight =-
7971.9 lb (3616 kg)

-= Best speed and FTH =-
373 mph at sea level (600 kmh) [1 min limit]
385 mph at 2500 ft (620 kmh at 800 m) [1 min limit]
423 mph at 20000 ft (681 kmh at 6100 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
4.3 min

-= Service Ceiling =-
39000 ft (11900 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
14.0 sec/lap, 569 ft radius

-= Power ON stall speed at 75% fuel =-
87 mph (140 kmh)

-= Max recommended dive speed (IAS) =-
460 mph (740 kmh)

-= Other notes =-
Elevator becomes heavy at high speeds. WEP can only be used for 1 min stints. 15 minutes water injection capacity per sortie.


In 1942, Nakajima began working on a successor to the Ki-43. A prototype for the new fighter flew in April of 1943 and the Ki-84 Hayate (Gale) was born.
 
Code named "Frank" by the allies, the Ki-84 began production by the end of the year, but trouble with the engines delayed any substantial delivery until the middle of 1944. The first Sentai (Squadron) was formed in April 1944 for service in China and by December a full 16 Sentai had been formed. The total production of Ki-84's was 3,382 aircraft including the initiaL model Ki-84-Ia, the Ki-84-Ib with two 20mm cannon replacing the machine guns in the nose, the Ki-84-Ic with 30mm cannon replacing the wing MG's, and a few Ki-84-II which used wood for some parts.
 
Only slightly slower than the P-51 and P-47, the Ki-84 outclimbed and outturned them both below 20,000 feet. It was a match for any allied aircraft it fought, and remained in service till the end of the war, though increased bombing of Japanese industry greatly affected production quantity and quality.