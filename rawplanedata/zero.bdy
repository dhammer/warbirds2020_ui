MITSUBISHI A6M3 MODEL 32 "REISEN" (ZERO)
-= Full weight =-
5690.8 lb (2581 kg)

-= Best speed and FTH =-
300 mph at sea level (483 kmh)
327 mph at 7500 ft (526 kmh at 2300 m)
350 mph at 19000 ft (563 kmh at 5800 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
5.7 min

-= Service Ceiling =-
35000 ft (10700 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
11.7 sec/lap, 371 ft radius

-= Power ON stall speed at 75% fuel =-
73 mph (117 kmh)

-= Max recommended dive speed (IAS) =-
420 mph (676 kmh)

-= Other notes =-
Better roll rate than the A6M2. Reduced aileron authority at high speeds. No pilot armor or self sealing fuel tanks.


The model -32 fielded the improved Nakajima NK1F Sakae 21 engine. Another difference from the A6M2 was the clipped wings which allowed for a better roll rate, while the disadvantage was slightly less lift from the wings. The A6M3 was not much heavier than the A6M2 and was an overall improvement as it had a faster top speed and more cannon ammo. Unfortunately though the Zeros did not evolve as well as the fighters of other countries, and had their golden era with the model 21. The A6m3 was still a potent fighter though if it could get it's opponent into a turn fight.

Designed in 1937, the Mitsubishi A6M "Zero" or "Zeke" fighter was revolutionary and unbeatable to its Chinese opponents when it was introduced in the summer of 1940. 15 pre-production test models all but ended all Chinese attempts to intercept Japanese bombers over mainland China. Designed as a carrier borne fighter from the start, the A6M was the best in the world during the first years of WW2. Had the Japanese won the early victory in the Pacific as they planned, the Zero may have been enough, but the war dragged on, and the Zero was soon outclassed by better and still better generations of enemy fighters whose numbers Japan could not match.
 
Production of the A6M series totalled approximately 10,449. The A6M2 was the model that first saw action in China, the A6M3 was introduced in July 1941, and the A6M5a saw service starting in Autumn, 1943.
 
Japanese pilots saw the plane as the modern successor to the Samurai's sword, and offensive capability was the main consideration. The Zero's turning ability was almost unmatched by any modern fighter, and it's range was no less than astounding. US planners frequently mistook far-ranging land based Zekes for signs of the presence of a Japanese carrier close by. Two 20mm cannon and two 7.7mm MG's gave it respectable firepower. But the Zero had it's shortcomings. The only front line naval fighter Japan used during most of the war, it's speed and climbing ability were no match for more modern fighters. It's light construction left it vulnerable to damage that would hardly affect the heavier opponents it faced, and even against the F4F, a plane it outperformed, this difference allowed superior team tactics by US fliers to defeat it. By the end of the war, Zero's were being used by Kamikaze pilots in the final defense of the home islands.
 
The top scoring Zero ace was Hiroyoshi Nishizawa. Nicknamed "The Devil" by his squadmates, he was credited with 113 victories although nobody knows an exact count for certain. Some figures place it lower, and some higher. His life came to an end when the transport plane he was flying in as a passenger was intercepted and shot down by American fighters.