YAKOVLEV YAK-9D
-= Full weight =-
6871.8 lb (3117 kg)

-= Best speed and FTH =-
332 mph at sea level (534 kmh)
342 mph at 2000 ft (550 kmh at 600 m)
367 mph at 13000 ft (591 kmh at 4000 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
5.7 min

-= Service Ceiling =-
33000 ft (10100 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
17.3 sec/lap, 767 ft radius

-= Power ON stall speed at 75% fuel =-
94 mph (151 kmh)

-= Max recommended dive speed (IAS) =-
404 mph (650 kmh)

-= Other notes =-
Limited to 1050 mmHg boost. Good roll rate. High wingloading but fairly agile. Limited ammunition.


While the Yak-3 and Yak-9D might look similar, the Yak-3 was a significantly smaller aircraft both in shape, wing area and weight compared to the Yak-9D. The Yak-3 was more agile, faster and climbed better while the Yak-9D had more fuel for long range missions. They both used the Klimov VK-105PF engine but the Yak-3 used a version of it which allowed for a better max boost. The 1945 Yakovlev version, the Yak-9U, was based on the heavier Yak-9D but used a much improved Klimov VK-107a engine, producing a maximum of 1650 hp compared to the 1300 hp of the Yak-3. The Yak-9U was considered the best in the series, and while being heavier and slightly less agile than the Yak-3, the stronger powerplant of the Yak-9U gave it a better top speed and power/weight ratio.

The Yak-9 was produced in larger numbers than any other Soviet fighter. Where the Yak-1 and Yak-3 were light point defense interceptors, the Yak-9, developed from the Yak-7, was a heavy multi-role aircraft.

With an increasing amount of materials being supplied by the United States, the Yak-7/Yak-9 line of fighters were able to make use of steel alloys and other materials unavailable to the designers of the Yak-1/Yak-3 line.

Production began in October 1942, with 16,769 Yak-9's of all variants produced, including 3,058 Yak-9d's.

The standard Yak-9 was powered by an M-105PF engine rated at 1,180 hp. It mounted one 20mm cannon through the nose and one 12.7mm machine gun in the cowl.

As the battles of 1943 continued on the Eastern front, Soviet ground forces more and more often advanced beyond the range of their air cover. The Yak-9d, with a longer range (about 870 miles) was produced to counter this problem. 

The Yak-9d was outstanding at low to medium altitudes, very much a match for the German fighters of the time. The only complaint is its very light armament.