Like its US "Dauntless" counterpart, the Aichi D3A (Code name: "Val") was an outmoded aircraft that surpassed its deficiencies in action. When the Japanese attacked Pearl Harbor, the first bombs dropped were by D3A's and the plane went on to considerable success in the first stages of conflict. It sank more Allied fighting ships than any other Axis aircraft.
 
The first prototype was completed and flown in late 1937 and won a production contract in December 1939 under the designation "Navy Type 99 Carrier Bomber Model 11". Production model D3A1's used a 1000 hp or 1070 hp engine and were outfitted with 2 forward and one rearward firing 7.7mm MG's. Normal payload was one 551 lb bomb under the fuselage with provision for 2 132 lb bombs on the wings. The D3A2 had a larger 1300 hp engine and increased fuel capacity along with other improvements and reached front line units by Autumn 1942. A total of 1,495 D3A's were produced.
 
In WarBirds, the Val is a scenario plane for the most part, and is comparable to the SBD in action.  
   
Armament:                     
                                      Ammo Load       
 
Primary  : 2 x 7.7mm MG's             791 rpg         
 
Secondary: Tail gunner 1 x 7.7mm MG   679 rpg      
 
Pilot MG's are fuselage mounted.