While it may have been the prettiest machine in W.W.I., the Albatross was plagued from it's first prototype with design issues and performance problems that would hound it until it was pulled from service.

The Albatross D.5-a, based on the D.III, saw its first service in 1917. With a Mercedes engine that brought it 158 horsepower, the underperforming airframe struggled against its Allied opponents. Still, in the hands of a master it could prove lethal. Richthofen himself flew one until he switched, shortly before his death, to the 3-winged Dr.1. His scathing review of the Albatross may have lead to Germany opening up more contracts with Fokker.

While the A model had some minor improvements over previous versions, the performance envelope was not nearly good enough to find dominance in front line service.
