---This Model is a work in Process---

The Air Force's predator Unmanned Aerial Vehicle made aerospace history 21 February 2001 as it successfully launched a live Hellfire missile helping it evolve from a non-lethal, reconnaissance asset to an armed, highly accurate tank killer.

Carrying Hellfire reduces endurance by a couple hours because of the drag, and especially if it is only carrying one because there is 100 pounds lopsided on the airplane. 
In late 2001 the Defense Department claimed a nearly "100 percent record of hits" in several dozen battlefield attacks by Predators in Afghanistan.

On 04 November 2002 six al-Qaida members traveling in a vehicle in Yemen were killed by a Hellfire missile fired by a CIA controlled Predator unmanned drone aircraft. A missile, reportedly fired by a CIA unmanned aircraft, hit the vehicle about 170 kilometers east of Yemen's capital Sana'a. All inside were killed. The MQ-1B was flown by a pilot on the ground in French-garrisoned Djibouti and overseen by commanders in Saudi Arabia.

A Jet-powered version of the predator B, The MQ-9A Predator B, flies faster, higher and carries more weapons than the Predator.
The Honeywell TP331-10 engine, producing 950 shp, provides a maximum airspeed of 260 kts and a cruise speed for maximum endurance of 150-170 kts. The MQ-9B can carry a payload mix of 1,500 lb. on each of its two inboard weapons stations, 500-600 lb. on the two middle stations and 150-200 lb. on the outboard stations. 
The first production MQ-9B had been built by late 2002, at which time three more were under construction, with 3-4 to follow in 2003 and full production of 9-15/year to be reached in 2004. 

Another version of the Predator B, with a 20-ft. wing extension, started flying in late 2002. The standard MQ-9, at a takeoff weight of 10,000 lb., can carry 3,000 lb. of payload and 3,000 lb. of fuel. With no exterior stores, it could stay aloft for 32 hr. at an altitude of more than 50,000 ft. 
The version with the wingspan extended to 86 ft., about the same as a 737 airliner, can carry 34 hr. of internal fuel. With two 1,000-lb. drop tanks and 1,000 lb. of weapons it can fly a 42-hr. mission. Payloads can vary, but a favorite is the steadily upgrading Lynx synthetic aperture radar with a range of about 15 mi. even through clouds and rain. 

