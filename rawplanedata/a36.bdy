NORTH AMERICAN A-36 "APACHE"
-= Full weight =-
9050 lb (4105 kg)

-= Best speed and FTH =-
359 mph at sea level (578 kmh)
367 mph at 2500 ft (591 kmh at 800 m)
362 mph at 12000 ft (583 kmh at 3700 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
7.4 min

-= Service Ceiling =-
29000 ft (8800 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
17.2 sec/lap, 670 ft radius

-= Power ON stall speed at 75% fuel =-
91 mph (146 kmh)

-= Max recommended dive speed (IAS) =-
480 mph (772 kmh)

-= Other notes =-
Agile at high speeds.


The A-36 Apache was North American's low altitude dive bombing version of the P-51 series.  The prototype, designated NA-73X, first flew in October 1940 with the first production models flying in May 1941. Although the early Allison engined models exhibited many fine characteristics and served as low altitude fighters, reconnaissance and dive bombers, the design came up short in its primary intended role as a high altitude interceptor.  With war looming but no money to be spent on "fighter" aircraft the Army Air Corp had the NA-73X built as a "dive bomber" and called it the A-36.

The A-36 was an excellent low altitude strike/dive bomber. It's Allison engine was specifically built to be a "sea level" rated engine without the superchargers needed for high alt fighter combat.