NORTH AMERICAN P-51B "MUSTANG"
-= Full weight =-
9774 lb (4433 kg)

-= Best speed and FTH =-
370 mph at sea level (595 kmh)
430 mph at 17000 ft (692 kmh at 5200 m)
435 mph at 26000 ft (700 kmh at 7900 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
5.7 min

-= Service Ceiling =-
40000 ft (12200 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
17.5 sec/lap, 707 ft radius

-= Power ON stall speed at 75% fuel =-
94 mph (151 kmh)

-= Max recommended dive speed (IAS) =-
480 mph (772 kmh)

-= Other notes =-
Long range. Agile at high speeds.


The North American P-51 Mustang is considered by many to be the finest single seat fighter of WW2. The P-51 was originally designed for the RAF. The prototype, designated NA-73X, first flew in October 1940 with the first production models flying in May 1941. Although the early Allison engined models exhibited many fine characteristics and served as low altitude fighters, reconnaissance and dive bombers, the design came up short in its primary intended role as a high altitude interceptor until the RAF tested the Rolls Royce Merlin 61 and 65 engines in it's Mustang I's. The improvements in performance were enough to impress the USAAF into ordering large numbers of the aircraft. The P-51 in all variants totaled over 15,000 produced, with the P-51D entering service in early 1944 and accounting for 7,956 of that number.
 
With it's outstanding range, the P-51 was able to accompany deep bomber strikes into Germany, a welcome change for the bomber crews who previously left their escorts behind long before entering the German heartland. The Mustang's speed and handling made it a match for anything the enemy put against it, and Mustangs were even credited with kills of the German Me-262 jet fighter late in the war.
 
In the Pacific, the P-51 performed equally as well, though dust and humidity made maintenance a problem. As in Europe, its great range made the Mustang stand out, allowing Mustangs to escort B-29 strikes against the Japanese home islands. 
 
The leading P-51 ace of the war was Major George Preddy Jr. with 26.83 victories. On Christmas day 1944, Preddy shot down two Bf-109's and was chasing a Fw-190 at tree-top level when he was shot down and killed by American anti-aircraft fire meant for the plane he was pursuing.