DE HAVILLAND DH.98 "MOSQUITO" NF.II
-= Full weight =-
19766 lb (8966 kg)

-= Best speed and FTH =-
340 mph at sea level (547 kmh)
366 mph at 6000 ft (589 kmh at 1800 m)
381 mph at 11000 ft (613 kmh at 3350 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
8.1 min

-= Service Ceiling =-
30000 ft (9150 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
18.4 sec/lap, 786 ft radius

-= Power ON stall speed at 75% fuel =-
99 mph (159 kmh)

-= Max recommended dive speed (IAS) =-
450 mph (724 kmh)

-= Other notes =-
Good controls at all speeds. Slow roll rate. 


The "Wooden Wonder" was rejected as a concept by Britain's Air Marshals, and had to be designed and built in an English manor house and test flown off an adjoining farm field. It became possibly the most successful and certainly most versatile British aircraft of WW2.

It served as Photo-recon, day fighter, night fighter, bomber, mine layer, torpedo bomber, tank buster, pathfinder, and submarine hunter. What's left?

Sir Geoffrey de Havilland proposed his wooden speed bomber in in 1938. When he was turned down, he said "We'll do it anyway". With the encouragement of Air Chief Marshal Sir Wilfred Freeman, de Havilland set up shop in Salisbury Hall and set to work. The prototype flew in November 1940 and went into production.

Wood was not a scarce commodity like the metals used in other planes, and much of the skilled workforce came from otherwise underemployed furniture craftsmen...making the Mosquito doubly valued, as much for its manufacturing efficiency as its performance.

7,781 Mosquitos of all types were eventually built. At near 400 mph level speed, it had the ability to penetrate deep into enemy territory while avoiding interception. In 1943, an 11am address in Berlin by Goering in which it was boasted that no enemy bomber would ever reach Berlin unscathed was interrupted by three Mosquitos from 105 squadron who dropped their bombs on the city and returned safely to English soil. At 4pm that same day, a similar address by Goebbels was interrupted in the same manner. It was an outstanding success, almost stage-managed by Goering himself, that gave the Mosquito a moral advantage it never relinquished.