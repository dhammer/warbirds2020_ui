SUPERMARINE SPITFIRE IIa
-= Full weight =-
6179 lb (2803 kg)

-= Best speed and FTH =-
304 mph at sea level (489 kmh)
362 mph at 14300 ft (583 kmh at 4400 m)
341 mph at 25000 ft (549 kmh at 7600 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
5.3 min

-= Service Ceiling =-
36000 ft (11000 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
11.8 sec/lap, 446 ft radius

-= Power ON stall speed at 75% fuel =-
76 mph (122 kmh)

-= Max recommended dive speed (IAS) =-
450 mph (724 kmh)

-= Other notes =-
Negative G's engine cutout. Agile with a gentle stall behaviour.


The Mk.IIa is equipped with the Merlin XII engine, rated at 1302 hp at 14300 ft. It has a similar max boost as the Merlin III up to 9000 ft, but has an improved high altitude performance. The Spit IIa also has more base BHP when flown at cruise or climb power, meaning that it is faster and climbs better than the Spit I when not in WEP. The Mk.IIa gave the English pilots a well needed boost in high altitude performance against the German Bf 109E's.

In addition to being one of the most beautiful fighters ever built, the Supermarine Spitfire was one of the most robust designs. It had a great wing efficiency due to the eliptical shape of it's wings, resulting in less induced drag effects. With the same airframe that first flew as a prototype in 1936, the Spitfire remained in production throughout WW2, the only Allied fighter to do so. Improvements were mainly to the engine, with the basic shape remarkably able to accommodate the increased size and power of the ever larger Merlins. Combining speed, firepower, and agility into one potent package, the Spit with it's distinctive elliptical wings is always remembered as the plane that won the Battle of Britain... though the Hawker Hurricane carried most of the load.
 
The Spitfire Mk1a was in service at the outbreak of WW2. The Spit V became available in March 1941, and the Spit IX, the most produced variant at 5,665 planes, was introduced in mid 1942.
 
James "Johnny" Johnson was the highest scoring British Spitfire pilot with 38 victories.