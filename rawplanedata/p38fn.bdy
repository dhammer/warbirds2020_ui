Developed by Kelly Johnson as a high altitude interceptor and escort fighter and built by Lockheed, the "Fork-Tailed Devil", or P-38, was an abrupt departure in design when its first prototype flew in 1939, and it continued to be an innovative aircraft through its illustrious career. With its twin engines, booms, and tails around a center fuselage, it was the first plane to enter squadron service with tricycle gear, all-metal flush riveted skin, turbo-supercharged engines, and power boosted controls. At the time it entered service it was the fastest and longest ranged fighter in the world.
 
Although it was hindered by technical and logistics problems and was gradually phased out in favor of the P-51 in Europe, the P-38 was an early mainstay in the Mediterranean and considered by many the premier fighter of the Pacific war.
 
America's two leading aces of WW2, Maj. Richard Bong (40 kills) and Maj. Tommy McGuire (38 kills) flew the P-38. Another P-38 claimed the life of Japan's Admiral Yamamoto when it shot down the transport carrying the Japanese commander. P-38's scored the final US air victories of the war when they downed six Ki-84 "Franks" on the day before the final ceasefire.
 
A total of 10,038 P-38's of all types were produced.
 
In WarBirds, the P-38 variants are always included in the list when speaking of "best fighters in the game". For beginners, its counter rotating props negate torque effects and its array of nose mounted armament removes convergence issues making it much easier to learn both the basics of ACM and gunnery. In the hands of an expert, the P-38L in particular is an awesome platform both in scenarios and in arena play with its combination of speed, maneuverability, guns, and toughness. The only drawback to this plane is its great size and distinctive shape. It's a big target, and is easy to see from a long way out. 
The 38F is an early version and the best turner of the three while the 38J is faster but enters compressibility easily in a dive. The 38L is the most modern, with power assisted controls that allow it to use its speed more effectively.
    
Armament:                     
                                   Ammo Load          
 
Primary  : 4 x .50 cal MG's        500 rpg            
 
Secondary: 1 x 20mm cannon         150 rpg            
 
All guns are nose mounted.
 
 
The "Ammo Load" is based on the historic normal operational ammo load for the plane/weapon(s) in question.