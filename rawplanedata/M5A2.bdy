The M5A2 is a mix of the old and the new. It still has the tried and true .50-caliber coaxial heavy machine gun, but can now be fitted with the RAVEN 20mm chain gun. It has an engine that runs on fossil fuels but its advanced fire control system gives it a level of accuracy that is unheard of. 

The M5 is a very powerful machine. It has both older weapons, like the M256 120mm smoothbore, and newer weapons, such as the RAVEN 20mm chain gun. This mix makes a deadly combination. Its state-of-the-art fire control system gives the Schwartzkopf accuracy previously unheard of, making rivals with even the T-100 Ogre, aka "The Tiger I of WWIII." The M5 would see action in EndWar, destroying scores of tanks.

Armament:                                           
1 x 37mm M6 main gun
1 x 0.30 caliber Browning M1919A4 coaxial machine gun.
1 x 0.30 caliber Browning M1919A4 Anti-Aircraft (AA) machine gun on turret side (operated externally). 

Ammunition load:
103 x 37mm projectiles
7,500 x 0.30 caliber ammunition
