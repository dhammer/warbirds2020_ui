HAWKER HURRICANE I
-= Full weight =-
6403.8 lb (2905 kg)

-= Best speed and FTH =-
287 mph at sea level (462 kmh)
323 mph at 9000 ft (520 kmh at 2750 m)
317 mph at 16200 ft (510 kmh at 4900 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
6.5 min

-= Service Ceiling =-
33000 ft (10100 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
11.1 sec/lap, 383 ft radius

-= Power ON stall speed at 75% fuel =-
72 mph (116 kmh)

-= Max recommended dive speed (IAS) =-
390 mph (628 kmh)

-= Other notes =-
Thick wings, high max angle of attack.


The Hurricane Mk I was powered by the Merlin III engine just like the Spitfire Mk I. This single stage engine could produce 1305 hp at 9000 ft. While the Spitfire was faster, the Hurricane had a tighter turning circle. This was a purebred fighter aircraft and was definitely competitive against any contemporary fighters.

Design of the Hawker Hurricane began in 1933 as Britain looked for its first monoplane fighter. A prototype was flown in November of 1935 and production of the Hurricane began in July 1936 with the first squadrons forming by the end of 1937. All Hurricanes had fabric covered wings until the Autumn of 1939 when the installation of the 1260 hp Merlin XX engine and heavier armament marked the Hurricane II series of variants. The IIa kept the old wings with the newer engine while the IIb had 12 browning machine guns mounted in the new all metal stressed skin wings. The IIc used four 20mm Hispano-Suiza drum fed cannon and the IId was outfitted with a pair of anti-tank cannons. 
 
The memorable Spitfire gets most of the credit for winning the Battle of Britain, but the Hawker Hurricane was the real workhorse of the conflict. The 1715 Hurricanes that served in the battle accounted for four fifths of the enemy aircraft destroyed. The Hurricane expanded it's role into the Mediterranean, North Africa, and the Middle East during 1940, and into the Far East in 1941. It saw air-to-ground action for the most part as later generations of fighters took up the air battle. A total of 14,223 Hurricanes were produced.
 
The top scoring Hurricane ace in WW2 was Squadron Leader Marmaduke Thomas St. John Pattle, a South African who served in North Africa and later in Greece. He had 35 victories while flying the Hurricane. Some estimates give him 50 kills altogether which would also make him the highest scoring RAF ace of the war.